subjects = {'001'; '003'; '004'; '005'; '007'; '008'; '011'; '013'; '014'; '016'; '017'; '019'; '020'; '021'; '022'; '023'};

%subjects = {'004'};

for ii = 1:length(subjects)
    
    display(['Subject ',subjects{ii,:}])
    dynvar = ['subject_',num2str(ii)];
    segment = 'induction'; % 'induction', 'emergence' or 'full'.
    
    % File and path settings
    eegpath = '/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/Raw_original/Preproc_250Hz/';
    markerpath = '/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/Raw_original/Preproc_250Hz/';
    
    eegfile = ['bench_',subjects{ii,:},'_filt_avg.vhdr'];
    markerfile = ['bench_',subjects{ii,:},'_filt_avg.vmrk'];
    fxconcfile = 'ESCsmooth.mat';
    load(fxconcfile);
    
    EEG = pop_loadbv(eegpath,eegfile); 
    EEG.data = double(EEG.data);

    fxconc = ESCsmooth(1:58*60); EEG.data = EEG.data(:,1:58*60*EEG.srate); 

    % Zero-phase Butterworth filter is applied.
    [BB, AA] = butter(3,[0.5 90]/EEG.srate/2,'bandpass');% filter
    SWA = zeros(32, 58*60);
    
    fprintf('SWA electrode: ') 
    %SWA = group.(dynvar).SWA;
    idx = [31, 32];
    idx = 1:32;
    counter = 1;
    % Mean in the slow-wave band is extracted for each electrode and sigmoid fitted.
    for el = [1:31]
        id = idx(counter);
        fprintf(string(el))
        data = EEG.data(el,:);
        filt = filtfilt(BB, AA, data);
        [s, w, ~, ps] = spectrogram(filt,4*EEG.srate,3*EEG.srate,0.25:0.25:90,EEG.srate,'power','yaxis');
        ps_db = 10*log10(ps);
        SWA(id, 1:length(ps_db(1,:))) = mean(ps_db(2:6,:));
        
        n_fxconc = length(fxconc);
        if (n_fxconc > size(SWA,2)); n_fxconc = size(SWA,2); end
        swa = SWA(id,1:n_fxconc);
        FXconc = fxconc(1:n_fxconc);
        %res = SWO_fit_v2019(swa, FXconc); 
        
        modelfun = @(p,x) p(1) + (p(2) - p(1)) ./ (1 + exp(-(x-p(3))/p(4)));
        p0 = [3, 20, 1.5, 0.1];
        res0 = fitnlm(FXconc(FXconc>0.01), swa(FXconc>0.01), modelfun, p0);
        res.p = table2array(res0.Coefficients(:,1));
%         If a failed fit, try to omit initial period
%         if ~isreal(res.TswasStart)
%             res = SWO_fit_v2019(swa(700:end), FXconc(700:end)); 
%             res = fitnlm(FXconc(700:end), swa(700:end), modelfun, p0);
%             res.TswasStart
%         end
        
        group.(dynvar).SWAfits_induction{id} = res;
        counter = counter + 1;
    end
    
    
    if (n_fxconc > size(SWA,2)); n_fxconc = size(SWA,2); end
    swa = SWA(:,1:n_fxconc);
    FXconc = fxconc(1:n_fxconc);
    
    group.(dynvar).SWA = swa;
    group.(dynvar).fxconc = FXconc;
%     group.(dynvar).Tlobr = group_old.(dynvar).Tlobr; %LOBR(ii);%EEG.lor/1000;
%     group.(dynvar).Clobr = group_old.(dynvar).Clobr;%FXconc(int64(LOBR(ii)));
     
   % Main properties are extracted for Fz
    swaFz = swa(17, :);
%     res = SWO_fit_v2019(swaFz, FXconc);  
%     t = res.c; u = res.d; b = res.b; a = res.a;
%     Cswas =  t -  u * log(b/(0.95*(a+b)-a) - 1);
    
    res0 = fitnlm(FXconc(FXconc>0.01), swaFz(FXconc>0.01), modelfun, p0);
    res.p = table2array(res0.Coefficients(:,1));
    r = res.p(1);
    s = res.p(2);
    t = res.p(3);
    u = res.p(4);
    a = r; b = s-r;
    Cswas =  t -  u * log(b/(0.95*(a+b)-a) - 1);
        
    %Cswas = res.TswasStart;
    Pswas = 0.95*(a + b);
    [~, argswas] = min(abs(FXconc - Cswas));
    Tswas = argswas;
    group.(dynvar).Cswas = Cswas;
    group.(dynvar).Pswas = Pswas;
    group.(dynvar).Tswas = Tswas;
    
    save('/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/group_bench_fmincon_25_11_22_abs.mat', 'group')
end
fail
%% Visually check fits and save figure
subjects = {'001'; '003'; '004'; '005'; '007'; '008'; '011'; '013'; '014'; '016'; '017'; '019'; '020'; '021'; '022'; '023'};
%subjects = {'023'};

outdir = '/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/Di_complexity/SWAS_fits_figs/';
load('/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/Di_complexity/prop_bench_el_locs.mat');
fprintf('Patient: \n')

for ii = 1:3%length(subjects)
    fprintf(string(ii))

    h = figure('units','normalized','outerposition',[0 0 1 1]);
    sgtitle(['Propofol indn P' subjects{ii} ' avg ref FMINCON'])
    ylabel('SWA [dB]')
    xlabel('Time [s]')
    grid on
    
    dynvar = ['subject_',num2str(ii)];
    swa = group.(dynvar).SWA;
    %Clobr = group.(dynvar).Clobr;
    fxconc = group.(dynvar).fxconc;  
        
    fprintf('SWA electrode: ') 
    
    for el = 1:30
        res = group.(dynvar).SWAfits_induction{el};
%         Pswas = res.Pswas;
%         Cswas = res.TswasStart;
       % fxconc = res.fxconc;
        
        params = res.p;
        r = res.p(1);
        s = res.p(2);
        t = res.p(3);
        u = res.p(4);
        a = r; b = s-r;
        pred = modelfun(res.p, fxconc);
        %t = res.c; u = res.d; b = res.b; a = res.a;
        Cswas =  abs(t -  u * log(b/(0.95*(a+b)-a) - 1));
        Pswas = 0.95*(a + b);
        fprintf(string(el))
        subplot(5, 6, el)
        dim = [.2 .5 .3 .3];
        str = loc{el, 1};
        plot(fxconc, swa(el, :), 'r.')
        hold on
        plot(fxconc, pred, 'g-', 'LineWidth', 4)
        title(str)%, 'location', 'northwest');
        ylim([-15 25]);
        
        if Cswas < 5 && isreal(Cswas)
            xline(Cswas, 'b-', 'C_{SWAS}', 'LineWidth', 4);
        end
        
        if prctile(swa(el, :), 99) > Pswas
            yline(Pswas, 'k-', 'P_{SWAS}', 'LineWidth', 4);
        end
        
        %xline(Clobr, 'c-', 'C_{LOBR}', 'LineWidth', 4);
        xlim([0, 4.5])
    end
    
    figName = [outdir 'prop_bench_ind_P' subjects{ii} '_avg_ref_FMINCON_fits.png'];
    saveas(h, figName)
    close(h)
    fprintf('\n')
end
