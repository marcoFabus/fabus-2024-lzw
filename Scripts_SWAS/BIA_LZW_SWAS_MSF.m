clear all

subs = {'001','003','004','005','007', '011', '008','013','014','016','017','019','020','021','022','023'};
%011

lLZW_path = 'E:\Datasets\propofol_bench\Di_complexity\preprocess_local_LZW\';
SWAS_path = 'E:\Datasets\propofol_bench\Di_complexity\';
lLZW_path = '/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/Di_complexity/preprocess_local_LZW_18_01_23/';
SWAS_path = '/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/Di_complexity/';
figure_path = '/home/marco/Insync/sjoh4485@ox.ac.uk/OneDrive Biz/PhD Year 2/Complexity_Di/Figures/';

eloc = '/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/prop_bench_el_locs.ced';
%eloc = 'E:\Datasets\propofol_bench\Di_complexity\prop_bench_el_locs.ced';
eeglab;
%% SWAS params
Nel = 31;
Ns = length(subs);
load([SWAS_path, 'group_bench_fmincon_25_11_22_abs.mat']);
%
r = zeros(Nel,Ns);
s = zeros(Nel,Ns);
s_min_r = zeros(Nel,Ns);
t = zeros(Nel,Ns);
u = zeros(Nel,Ns);
Cswas = zeros(Nel,Ns);
Pswas = zeros(Nel,Ns);
failed_subs = zeros(Ns, 1);

for ii = 1:length(subs)
    failed = 0;
    dynvar = ['subject_', num2str(ii)];
    for el = 1:Nel
        res = group.(dynvar).SWAfits_induction{el};
        rr = res.p(1);
        ss = res.p(2);
        tt = res.p(3);
        uu = res.p(4);
        a = rr; b = ss-rr;
        Cswas0 =  tt -  uu * log(b/(0.95*(a+b)-a) - 1);
        res.TswasStart = Cswas0;
        res.Pswas = 0.95*(a + b);
        % Check if we have a failed fit - Cswas above concentration reached or bottom 1%
        if res.TswasStart > 4.25 || res.TswasStart < 0.8041% || ~isreal(res.TswasStart)
            failed = failed + 1; 
            r(el,ii) = NaN;
            s(el,ii) = NaN;
            t(el,ii) = NaN;
            u(el,ii) = NaN;
            s_min_r(el, ii) = NaN;
            Cswas(el,ii) = NaN;
            Pswas(el,ii) = NaN;
            continue
        end
        
        r(el,ii) = res.p(1);
        s(el,ii) = res.p(2);%+res.p(1);
        t(el,ii) = res.p(3);
        u(el,ii) = res.p(4);
        s_min_r(el, ii) = res.Pswas - res.p(1);
        Cswas(el,ii) = real(res.TswasStart);
        Pswas(el,ii) = res.Pswas; 
        
    end
    
    % Exclude subjects if SWAS not achieved in >1/3 of electrodes
    if failed > 4
        r(:, ii) = NaN;
        s(:, ii) = NaN;
        t(:, ii) = NaN;
        u(:, ii) = NaN;
        s_min_r(:, ii) = NaN;
        Cswas(:, ii) = NaN;
        Pswas(:, ii) = NaN;
        failed_subs(ii) = 1;
    end
end
sum(failed_subs)
gm_r = nanmean(r,2);
gm_s = nanmean(s,2);
gm_t = nanmean(t,2);
gm_u = nanmean(u,2);
gm_s_min_r = nanmean(s_min_r,2);
gm_Pswas = nanmean(Pswas,2);
gm_Cswas = nanmean(Cswas,2);

% Show subject variability in Cswas range across scalp. Note it's not
% normally distributed, so median pm MAD given.
Cswas_range = range(Cswas);
Cswas_range_median = nanmedian(Cswas_range);
Cswas_range_MAD = mad(Cswas_range, 1);
fail
%% Broadband
% Load full concentration
%fxconcpath = '/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/ESCsmooth.mat';
%fxconcpath = 'E:\Datasets\propofol_bench\ESCsmooth.mat';
fxconcpath = 'ESCsmooth.mat';
load(fxconcpath)
fxconc = ESCsmooth(1:58*60);
    
% Load parameters. Baseline = first 10 minutes, SWAS = from SWAS to 68min
bl_broad_rho0 = zeros(Nel,120,Ns);
swas_broad_rho0 = zeros(Nel,Ns)*NaN;
failed = 0;
for ii = 1:Ns
    dynvar = ['subject_', num2str(ii)];
    load([lLZW_path, 'propofol_' subs{ii} '_bench_all_ocICA_avg_prep_local_broad_LZW.mat']);
    
    % Get Fz Cswas
    res = group.(dynvar).SWAfits_induction{17};
    rr = res.p(1);
    ss = res.p(2);
    tt = res.p(3);
    uu = res.p(4);
    a = rr; b = ss-rr;
    Cswas0 =  tt -  uu * log(b/(0.95*(a+b)-a) - 1);
    
    %Cswas0 = group.(dynvar).Cswas;
    [~, Tswas] = min(abs(fxconc - Cswas0));
    if Cswas0 > 4.5 || ~isreal(Cswas0)
         failed = failed +1;
        continue
    end
    
    lzw_swas_ix = int64(Tswas / 5);
    bl_broad_rho0(:,:,ii) = rho0_broad(1:Nel,1:120);
    swas_broad_rho0(:,ii) = mean(rho0_broad(1:Nel,lzw_swas_ix:lzw_swas_ix+120), 2);
end

m_bl_broad_rho0 = mean(bl_broad_rho0,2);
m_bl_broad_rho0 = squeeze(m_bl_broad_rho0);

m_swas_broad_rho0 = swas_broad_rho0;
%m_swas_broad_rho0 = squeeze(m_swas_broad_rho0);

gm_bl_broad_rho0 = nanmean(m_bl_broad_rho0,2);
gm_swas_broad_rho0 = nanmean(m_swas_broad_rho0,2);

% Find correlations and p values between SWAS and LZW parameters
for ii = 1:length(m_bl_broad_rho0)
    [gr_r_broad(ii),gp_r_broad(ii)] = corr(m_bl_broad_rho0(ii,:)',r(ii,:)','rows','complete',  'Type', 'Spearman');
    [gr_s_broad(ii),gp_s_broad(ii)] = corr(m_swas_broad_rho0(ii,:)',s(ii,:)','rows','complete',  'Type', 'Spearman');
    [gr_s_min_r_broad(ii),gp_s_min_r_broad(ii)] = corr(m_swas_broad_rho0(ii,:)'-m_bl_broad_rho0(ii,:)',Pswas(ii,:)'-r(ii,:)','rows','complete',  'Type', 'Spearman');
    [gr_t_broad(ii),gp_t_broad(ii)] = corr(m_swas_broad_rho0(ii,:)',t(ii,:)','rows','complete',  'Type', 'Spearman');
    [gr_u_broad(ii),gp_u_broad(ii)] = corr(m_swas_broad_rho0(ii,:)',u(ii,:)','rows','complete',  'Type', 'Spearman');
    [gr_Pswas_broad(ii),gp_Pswas_broad(ii)] = corr(m_swas_broad_rho0(ii,:)',Pswas(ii,:)','rows','complete',  'Type', 'Spearman');
    [gr_Cswas_broad(ii),gp_Cswas_broad(ii)] = corr(m_swas_broad_rho0(ii,:)',Cswas(ii,:)','rows','complete',  'Type', 'Spearman');
    [gr_Cswas_Pswas(ii),gp_Cswas_Pswas(ii)] = corr(Pswas(ii,:)',Cswas(ii,:)','rows','complete',  'Type', 'Spearman');

end

%% SWAS parameters figure

f = figure;
set(gca,'fontsize',16); 

subplot(1,4,1)
topoplot(gm_r,eloc,'plotchans', 1:Nel, 'maplimits', 'maxmin'); 
title('(A) Baseline','fontsize',16);
c = colorbar('southoutside', 'Ticks', [-10, -5, 0, 5, 10, 15]); 
c.Label.String = 'Slow-wave Power [dB]';
c.Position = c.Position + [0 -0.2 0. 0.025];
caxis([-10, 15])
set(gca,'fontsize',16); 

subplot(1,4,2)
topoplot(gm_Pswas,eloc,'plotchans', 1:Nel, 'maplimits', 'maxmin'); 
title('(B) SWAS','fontsize',16);
c = colorbar('southoutside', 'Ticks', [-10, -5, 0, 5, 10, 15]); 
c.Label.String = 'Slow-wave Power [dB]';
c.Position = c.Position + [0 -0.2 0. 0.025];
caxis([-10, 15])
set(gca,'fontsize',16); 

subplot(1,4,3)
topoplot(gm_s_min_r,eloc,'plotchans', 1:Nel, 'maplimits', 'maxmin'); 
title('(C) SWAS - Baseline','fontsize',16);
c = colorbar('southoutside', 'Ticks', [5, 10, 15]); 
c.Label.String = 'Slow-wave Power [dB]';
c.Position = c.Position + [0 -0.2 0. 0.025];
caxis([5, 16])
set(gca,'fontsize',16); 

subplot(1,4,4)
topoplot(gm_Cswas,eloc,'plotchans', 1:Nel, 'maplimits', [3., 4]); 
title('(D) C_{SWAS}','fontsize',16);
c = colorbar( 'southoutside', 'Ticks', [2, 2.5, 3, 3.5]); 
c.Label.String = 'Propofol [�g/ml]';
caxis([2, 3.5])
c.Position = c.Position + [0 -0.2 0. 0.025];
set(gca,'fontsize',16); 
titleHandle = get( gca ,'Title' );
pos  = get( titleHandle , 'position' );
pos1 = pos + [-0. -0.08 0]; 
set( titleHandle , 'position' , pos1 );

f.Position(3:4) = [1200,400];
%exportgraphics(f,[figure_path,'Fig_SWAS_params_fmincon_v2_new.png'], 'Resolution', 300)
fail
%% SWAS parameters figure - with t and u

f = figure;
set(gca,'fontsize',16); 

subplot(2,3,1)
topoplot(gm_r,eloc,'plotchans', 1:Nel, 'maplimits', 'maxmin'); 
title('(A) Baseline','fontsize',16);
c = colorbar('southoutside', 'Ticks', [-10, -5, 0, 5, 10]); 
c.Label.String = 'Slow-wave Power [dB]';
%c.Position = c.Position + [0 -0.2 0. 0.025];
caxis([-10, 10])
set(gca,'fontsize',16); 

subplot(2,3,2)
topoplot(gm_Pswas,eloc,'plotchans', 1:Nel, 'maplimits', 'maxmin'); 
title('(B) SWAS','fontsize',16);
c = colorbar('southoutside', 'Ticks', [-10, -5, 0, 5, 10]); 
c.Label.String = 'Slow-wave Power [dB]';
%c.Position = c.Position + [0 -0.2 0. 0.025];
caxis([-10, 10])
set(gca,'fontsize',16); 

subplot(2,3,3)
topoplot(gm_s_min_r,eloc,'plotchans', 1:Nel, 'maplimits', 'maxmin'); 
title('(C) SWAS - Baseline','fontsize',16);
c = colorbar('southoutside', 'Ticks', [10, 12, 14, 16]); 
c.Label.String = 'Slow-wave Power [dB]';
%c.Position = c.Position + [0 -0.2 0. 0.025];
caxis([10, 16])
set(gca,'fontsize',16); 

subplot(2,3,4)
topoplot(gm_Cswas,eloc,'plotchans', 1:Nel, 'maplimits', [3., 4]); 
title('(D) C_{SWAS}','fontsize',16);
c = colorbar( 'southoutside', 'Ticks', [3., 3.5, 4]); 
c.Label.String = 'Propofol [�g/ml]';
caxis([3, 4])
%c.Position = c.Position + [0 -0.2 0. 0.025];
set(gca,'fontsize',16); 

subplot(2,3,5)
topoplot(gm_t,eloc,'plotchans', 1:Nel, 'maplimits', [2.5, 3]); 
title('(E) t','fontsize',16);
c = colorbar( 'southoutside', 'Ticks', [2.5, 2.75, 3]); 
c.Label.String = 't [�g/ml]';
caxis([2.5, 3])
%c.Position = c.Position + [0 -0.2 0. 0.025];
set(gca,'fontsize',16); 

subplot(2,3,6)
topoplot(gm_u,eloc,'plotchans', 1:Nel, 'maplimits', [0.2, 0.3]); 
title('(F) SWAS gradient (u)','fontsize',16);
c = colorbar( 'southoutside', 'Ticks', [0.2, 0.25, 0.3]); 
c.Label.String = 'u [�g/ml]';
caxis([0.2, 0.3])
%c.Position = c.Position + [0 -0.2 0. 0.025];
set(gca,'fontsize',16); 

f.Position(3:4) = [1200,1200];
%exportgraphics(f,[figure_path,'Fig_SWAS_params_fmincon_all_v2.png'], 'Resolution', 300)

%% Example SWAS fit figure
f = figure; hold on
set(gca,'fontsize',16); 
el = 17;
swa = group.subject_2.SWA(el, :);
res = group.subject_2.SWAfits_induction{el};
baseline = mean(res.pred(1:600));
esc = group.subject_2.fxconc;
scatter(esc, swa, 'red', 'filled', 'MarkerFaceAlpha',.25,'MarkerEdgeAlpha',.25);
plot(esc, res.pred, 'b-', 'LineWidth', 6)
xl = xline(res.TswasStart, 'k-.', 'C_{SWAS}', 'LineWidth', 2, 'FontSize', 18);
xl.LabelVerticalAlignment = 'bottom';
xl.Annotation.LegendInformation.IconDisplayStyle = 'off';
yl = yline(res.Pswas, 'k-', 'P_{SWAS}', 'LineWidth', 2, 'FontSize', 18);
yl.LabelHorizontalAlignment = 'left';
yl = yline(baseline, 'k--', 'Baseline', 'LineWidth', 2, 'FontSize', 18);
yl.LabelHorizontalAlignment = 'left';
yl.LabelVerticalAlignment = 'bottom';
xlabel('Propofol [�g/ml]')
ylabel('Slow-wave Power [dB]')
legend('Data', 'Model fit', 'Location', 'north', 'NumColumns', 2)
ylim([-12, 30])
exportgraphics(f,[figure_path,'Fig_SWAS_model_v3.png'], 'Resolution', 300)

%% LZW vs SWAS correlations figure
f = figure;
subplot(1,3,1)
[p_fdr,p_masked] = fdr(gp_r_broad,0.05);
eloc_sig = find(p_masked);
topoplot(gr_r_broad,eloc,'maplimits',[-1,1],'emarker',{'.','k',20},'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:Nel); 
title('(A) Baseline','fontsize',24);

subplot(1,3,2)
[p_fdr,p_masked] = fdr(gp_Pswas_broad,0.05);
eloc_sig = find(p_masked);
topoplot(gr_Pswas_broad,eloc,'maplimits',[-1,1],'emarker',{'.','k',20},'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:Nel); 
title('(B) SWAS','fontsize',24);

subplot(1,3,3)
[p_fdr,p_masked] = fdr(gp_Cswas_broad,0.05);
eloc_sig = find(p_masked);
topoplot(gr_Cswas_broad,eloc,'maplimits',[-1,1],'emarker',{'.','k',20},'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:Nel); 
title('(C) C_{SWAS}','fontsize',24);
titleHandle = get( gca ,'Title' );
pos  = get( titleHandle , 'position' );
pos1 = pos + [-0. -0.08 0]; 
set( titleHandle , 'position' , pos1 );

c = cbar(); 
set(gca,'fontsize',16);
f.Position(3:4) = [1200,400];
%exportgraphics(f,[figure_path,'Fig_SWAS_vs_LZW_fmincon_v3.png'], 'Resolution', 300)



%% Combined old Fig 5 and 6 - SWAS and SWAS vs LZW
f = figure;
set(gca,'fontsize',16); 

subplot(2,4,1)
topoplot(gm_r,eloc,'plotchans', 1:Nel, 'maplimits', 'maxmin'); 
title('(i) Baseline','fontsize',16);
c = colorbar('southoutside', 'Ticks', [-10, -5, 0, 5, 10, 15]); 
c.Label.String = 'Slow-wave Power [dB]';
c.Position = c.Position + [0 -0.06 0. 0.01];
caxis([-10, 15])
set(gca,'fontsize',16); 

subplot(2,4,2)
topoplot(gm_Pswas,eloc,'plotchans', 1:Nel, 'maplimits', 'maxmin'); 
title('(ii) P_{SWAS}','fontsize',16);
c = colorbar('southoutside', 'Ticks', [-10, -5, 0, 5, 10, 15]); 
c.Label.String = 'Slow-wave Power [dB]';
c.Position = c.Position + [0 -0.06 0. 0.01];
caxis([-10, 15])
set(gca,'fontsize',16); 

subplot(2,4,3)
topoplot(gm_s_min_r,eloc,'plotchans', 1:Nel, 'maplimits', 'maxmin'); 
title('(iii) P_{SWAS} - Baseline','fontsize',16);
c = colorbar('southoutside', 'Ticks', [5, 10, 15]); 
c.Label.String = 'Slow-wave Power [dB]';
c.Position = c.Position + [0 -0.06 0. 0.01];
caxis([5, 16])
set(gca,'fontsize',16); 

% - Build title axes and title.
axes( 'Position', [0.03, 0.93, 1, 0.05] ) ;
set( gca, 'Color', 'None', 'XColor', 'White', 'YColor', 'White' ) ;
text( 0.5, 0,'(A) Slow-wave Parameter Topography', 'FontSize', 24', 'FontWeight', 'Bold', ...
  'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom' ) ;

subplot(2,4,4)
topoplot(gm_Cswas,eloc,'plotchans', 1:Nel, 'maplimits', [3., 4]); 
title('(iv) C_{SWAS}','fontsize',16);
c = colorbar( 'southoutside', 'Ticks', [2, 2.5, 3., 3.5]); 
c.Label.String = 'Propofol [�g/ml]';
caxis([2, 3.5])
c.Position = c.Position + [0 -0.06 0. 0.01];
set(gca,'fontsize',16); 
titleHandle = get( gca ,'Title' );
%pos  = get( titleHandle , 'position' );
%pos1 = pos + [-0. -0.08 0]; 
set( titleHandle , 'position' , pos1 );

subplot(2,4,5)
[p_fdr,p_masked] = fdr(gp_r_broad,0.05);
eloc_sig = find(p_masked);
topoplot(gr_r_broad,eloc,'maplimits',[-1,1],'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:Nel); 
title('(i) Baseline','fontsize',16);
set(gca,'fontsize',16);

subplot(2,4,6)
[p_fdr,p_masked] = fdr(gp_Pswas_broad,0.05);
eloc_sig = find(p_masked);
topoplot(gr_Pswas_broad,eloc,'maplimits',[-1,1],'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:Nel); 
title('(ii) P_{SWAS}','fontsize',16);
set(gca,'fontsize',16);
c = colorbar( 'southoutside', 'Ticks', [-1, -0.5, 0, 0.5, 1]); 
c.Label.String = 'Spearman''s \rho';
c.Position = c.Position + [0. -0.06 0.2 0.01];

subplot(2,4,7)
[p_fdr,p_masked] = fdr(gp_s_min_r_broad,0.05);
eloc_sig = find(p_masked);
topoplot(gr_s_min_r_broad,eloc,'maplimits',[-1,1],'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:Nel); 
title('(iii) P_{SWAS} - Baseline','fontsize',16);
set(gca,'fontsize',16);

subplot(2,4,8)
[p_fdr,p_masked] = fdr(gp_Cswas_broad,0.05);
eloc_sig = find(p_masked);
topoplot(gr_Cswas_broad,eloc,'maplimits',[-1,1],'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:Nel); 
title('(iv) C_{SWAS}','fontsize',16);
titleHandle = get( gca ,'Title' );
pos  = get( titleHandle , 'position' );
pos1 = pos + [-0. -0.08 0]; 
set( titleHandle , 'position' , pos1 );
set(gca,'fontsize',16);

% - Build title axes and title.
axes( 'Position', [0.015, 0.46, 1, 0.05] ) ;
set( gca, 'Color', 'None', 'XColor', 'White', 'YColor', 'White' ) ;
text( 0.5, 0,'(B) Slow Waves vs LZW', 'FontSize', 24', 'FontWeight', 'Bold', ...
  'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom' ) ;

f.Position(3:4) = [1200,850];
exportgraphics(f,[figure_path,'Fig_SWAS_vs_LZW_29_03_23.png'], 'Resolution', 300)

%% Cswas variation ANOVA
Electrode = 1:31;
T = array2table(Cswas.');
rm = fitrm(T, 'Var1-Var31 ~ 1', 'WithinDesign', Electrode);
ranovatbl = ranova(rm)
tbl = multcompare(rm,'Time');

figure;
for i = 1:Ns
    subplot(4,4,i)
    cswas = Cswas(:, i);
    chs = 1:Nel;
    chs = chs(~isnan(cswas));
    %cswas = cswas(~isnan(cswas));
    if isempty(chs)
        continue
    end
    topoplot(cswas,eloc,'plotchans', chs, 'maplimits', [prctile(cswas, 1), prctile(cswas, 99)]); 
    colorbar
end
tbl2 = tbl(table2array(tbl(:, 5))<0.05, :);
%% Spatial correlations
[R_r_spat_broad, p_r_spat_broad] = corr(gm_bl_broad_rho0,gm_r,'rows','complete',  'Type', 'Spearman');
[R_s_spat_broad,p_s_spat_broad] = corr(gm_swas_broad_rho0,gm_s,'rows','complete',  'Type', 'Spearman');
[R_s_min_r_spat_broad, p_s_min_r_spat_broad] = corr(gm_swas_broad_rho0-gm_bl_broad_rho0,gm_s_min_r,'rows','complete',  'Type', 'Spearman');
[R_Pswas_spat_broad,p_Pswas_spat_broad] = corr(gm_swas_broad_rho0,gm_Pswas,'rows','complete',  'Type', 'Spearman');
[R_Cswas_spat_broad,p_Cswas_spat_broad] = corr(gm_swas_broad_rho0,gm_Cswas,'rows','complete',  'Type', 'Spearman');

% Cswas vs power parameters
[R_Cswas_smr_spat_broad,p_Cswas_smr_spat_broad] = corr(gm_s_min_r,gm_Cswas,'rows','complete',  'Type', 'Spearman');
[R_Cswas_Pswas_spat_broad,p_Cswas_Pswas_spat_broad] = corr(gm_Pswas,gm_Cswas,'rows','complete',  'Type', 'Spearman');
[R_Cswas_r_spat_broad,p_Cswas_r_spat_broad] = corr(gm_r,gm_Cswas,'rows','complete',  'Type', 'Spearman');

Rs = [R_r_spat_broad, R_s_spat_broad, R_s_min_r_spat_broad, R_Pswas_spat_broad, R_Cswas_spat_broad, ...
    R_Cswas_smr_spat_broad, R_Cswas_Pswas_spat_broad, R_Cswas_r_spat_broad];
Ps = [p_r_spat_broad, p_s_spat_broad, p_s_min_r_spat_broad, p_Pswas_spat_broad, p_Cswas_spat_broad, ...
    p_Cswas_smr_spat_broad, p_Cswas_Pswas_spat_broad, p_Cswas_r_spat_broad];

%% GABA receptor density figures
load('/home/marco/Insync/sjoh4485@ox.ac.uk/OneDrive Biz/PhD Year 2/Complexity_Di/GABA/GABA_density_bench.mat')
gd10 = table2array(GABAdensitybench(:, 3));
gd10 = gd10(1:31);
% Exclude midline and TP
gd10([17:20, 29:31]) = NaN;
% Plot topographical maps and correlation

[R_Cswas_gd, p_Cswas_gd] = corr(gm_Cswas, gd10,  'Type', 'Spearman', 'rows', 'complete')
[R_rho0_gd, p_rho0_gd] = corr(gm_swas_broad_rho0, gd10,  'Type', 'Spearman', 'rows', 'complete');
[R_Pswas_gd, p_Pswas_gd] = corr(gm_Pswas, gd10,  'Type', 'Spearman', 'rows', 'complete');
[R_sminr_gd, p_sminr_gd] = corr(gm_s_min_r, gd10,  'Type', 'Spearman', 'rows', 'complete');
[R_r_gd, p_r_gd] = corr(gm_r, gd10,  'Type', 'Spearman', 'rows', 'complete');
[R_rho0b_gd, p_rho0b_gd] = corr(gm_bl_broad_rho0, gd10,  'Type', 'Spearman', 'rows', 'complete');

% Bonferroni correct
Rs = [R_r_gd, R_sminr_gd, R_Cswas_gd, R_Pswas_gd, R_rho0_gd, R_rho0b_gd]
p_gd = [p_r_gd, p_sminr_gd, p_Cswas_gd, p_Pswas_gd, p_rho0_gd, p_rho0b_gd]
p_gd_corr = bonf_holm(p_gd);
p_gd_corr = pval_adjust(p_gd, 'bonferroni')

%%
f = figure;
subplot(2, 3, 1)
topoplot(gd10, eloc, 'plotchans', [1:Nel], 'maplimits', [500, 800]); 
%tit = title('A              ','fontsize',40);
% c = colorbar( 'southoutside', 'Ticks', [500, 600, 700, 800]); 
% c.Label.String = 'GABA_AR density [pmol/ml]';
caxis([500, 800])
% c.Position = c.Position + [-0.025 -0.09 0.05 0.01];
% set(c,'fontsize',14); 
%pos  = get(tit, 'position' );
%pos1 = pos + [-0. -0.2 0]; 
%set(tit , 'position' , pos1 );

subplot(2, 3, 2)
topoplot(gm_Cswas,eloc,'plotchans', [1:16, 21:28], 'maplimits', [3., 4]); 
%title('(B) C_{SWAS}','fontsize',16);
%tit = title('B              ','fontsize',40);
% c = colorbar( 'southoutside', 'Ticks', [3., 3.5, 4]); 
% c.Label.String = 'Propofol C_{SWAS} [�g/ml]';
caxis([2.25, 3.25])
% c.Position = c.Position + [-0.025 -0.09 0.05 0.01];
% set(c,'fontsize',14); 
%pos  = get(tit, 'position' );
%pos1 = pos + [-0. -0.2 0]; 
%set(tit , 'position' , pos1 );

subplot(2, 3, 3, 'OuterPosition', [0.65, 0.5, 0.3, 0.4625])
plot(gd10, gm_Cswas, 'k.', 'MarkerSize', 15)
xlabel('GABA_AR density [pmol/ml]')
ylabel('C_{SWAS} [�g/ml]')
%title('(C) C_{SWAS} vs GABA_AR','fontsize',16);
set(gca,'fontsize',16); 
%yticks([3.3,  3.4, 3.5, 3.6, 3.7])
%tit = title('C              ','fontsize',40);
%pos  = get(tit, 'position' );
%pos1 = pos + [-110 0.0075 0]; 
%set(tit , 'position' , pos1 );
ylim([2.25, 3.15])

mdl = fitlm(gd10(~isnan(gd10)),gm_Cswas(~isnan(gd10)), 'y ~ x1');
ypred = predict(mdl, gd10);
P = polyfit(gd10(~isnan(gd10)),gm_Cswas(~isnan(gd10)),1);
yfit = P(1)*gd10+P(2);
hold on;
%plot(gd10,ypred,'r.', 'LineWidth', 5);
plot(gd10,yfit,'r-', 'LineWidth', 5);

dim = [.73 .3 .4 .4];
str = {['rho=-0.69'], ['P=0.0018']};
annotation('textbox',dim,'String',str,'FitBoxToText','on', 'FontSize',16,'EdgeColor','w', 'FontWeight', 'bold');

hold off

subplot(2, 3, 4)
topoplot(gd10, eloc, 'plotchans', [1:16, 21:28], 'maplimits', [500, 800]); 
%title('(D) GABA_A receptor density','fontsize',16);
tit = title('A              ','fontsize',40);
c = colorbar( 'southoutside', 'Ticks', [500, 600, 700, 800]); 
c.Label.String = 'GABA_AR density [pmol/ml]';
caxis([500, 800])
c.Position = c.Position + [-0.025 -0.09 0.05 0.01];
set(c,'fontsize',14); 
pos  = get(tit, 'position' );
pos1 = pos + [-0. -0.2 0]; 
set(tit , 'position' , pos1 );

subplot(2, 3, 5)
topoplot(gm_swas_broad_rho0,eloc,'plotchans', [1:16, 21:28]); 
%titleHandle = title('(E) Peak LZW','fontsize',20);
tit = title('B              ','fontsize',40);
c = colorbar( 'southoutside', 'Ticks', [0.6, 0.65, 0.7]); 
c.Label.String = 'Peak LZW Complexity';
%caxis([0.6, 0.7])
c.Position = c.Position + [-0.025 -0.09 0.05 0.01];
% pos  = get( titleHandle , 'position' );
% pos1 = pos + [-0. 0.01 0]; 
% set( titleHandle , 'position' , pos1 );
set(c,'fontsize',14); 
pos  = get(tit, 'position' );
pos1 = pos + [-0. -0.2 0]; 
set(tit , 'position' , pos1 );

subplot(2, 3, 6, 'OuterPosition', [0.65, 0.025, 0.3, 0.4625])
plot(gd10, gm_swas_broad_rho0, 'k.', 'MarkerSize', 15)
xlabel('GABA_AR density [pmol/ml]')
ylabel('Peak LZW')
%title('(F) LZW vs GABA_AR','fontsize',16);
set(gca,'fontsize',16);
%ylim([0.615, 0.675]);
%yticks([0.63, 0.65, 0.67]);
P = polyfit(gd10(~isnan(gd10)),gm_swas_broad_rho0(~isnan(gd10)),1);
yfit = P(1)*gd10+P(2);
hold on;
plot(gd10,yfit,'r-', 'LineWidth', 5);
hold off
tit = title('F              ','fontsize',40);
pos  = get(tit, 'position' );
pos1 = pos + [-110 -0.011 0]; 
set(tit , 'position' , pos1 );

f.Position(3:4) = [1200,750];
%exportgraphics(f,[figure_path,'Fig_GABAAR_v3_new.png'], 'Resolution', 400)


%% Paper plot: just 3
f = figure;
h1 = subplot(1, 3, 1);
topoplot(gd10, eloc, 'plotchans', [1:Nel], 'maplimits', [500, 800], 'numcontour', 6); 
c = colorbar( 'southoutside', 'Ticks', [500, 600, 700, 800]); 
c.Label.String = 'GABAr density [pmol/ml]';
c.Label.FontSize = 16;
caxis([500, 800])
c.Position = c.Position + [-0.06 -0.15 0. 0.025];
set(gca,'fontsize',16); 
title('A','fontsize', 32);
h1_pos=get(h1,'Position'); 
set(h1,'Position',[0.1, 0.25, .15, .6])
set(gca,'Units','normalized')
titleHandle = get( gca ,'Title' );
pos  = get( titleHandle , 'position' );
pos1 = pos + [-0.75 -0.2 0]; 
set( titleHandle , 'position' , pos1 );

h2 = subplot(1, 3, 2);
plot(gd10, gm_Cswas, 'k.', 'MarkerSize', 12)
xlabel(['GABAr density', newline, '[pmol/ml]'])
ylabel('C_{SWAS} [�g/ml]')
set(gca,'fontsize',16); 

P = polyfit(gd10(~isnan(gd10)),gm_Cswas(~isnan(gd10)),1);
yfit = P(1)*gd10+P(2);
hold on;
plot(gd10,yfit,'r-', 'LineWidth', 4);
hold off
xlim([500, 800])
ylim([3.35 3.85])
yticks([3.4, 3.6, 3.8])
h2_pos=get(h2,'Position'); 
set(h2,'Position',[0.4, 0.25, .15, .6])

title('B','fontsize',32);
set(gca,'Units','normalized')
titleHandle = get( gca ,'Title' );
pos  = get( titleHandle , 'position' );
pos1 = pos + [-325 -0.085 0]; 
set( titleHandle , 'position' , pos1 );


h3 = subplot(1, 3, 3);
plot(gd10, gm_swas_broad_rho0, 'k.', 'MarkerSize', 12)
xlabel(['GABAr density', newline, '[pmol/ml]'])
ylabel('Peak LZW')
set(gca,'fontsize',16);

P = polyfit(gd10(~isnan(gd10)),gm_swas_broad_rho0(~isnan(gd10)),1);
yfit = P(1)*gd10+P(2);
hold on;
plot(gd10,yfit,'r-', 'LineWidth', 4);
hold off
xlim([500, 800])
ylim([0.615, 0.68])
h3_pos=get(h3,'Position'); 
set(h3,'Position',[0.7, 0.25, .15, .6])

title('C','fontsize',32);
set(gca,'Units','normalized')
titleHandle = get( gca ,'Title' );
pos  = get( titleHandle , 'position' );
pos1 = pos + [-325 -0.011 0];
set( titleHandle , 'position' , pos1 );


f.Position(3:4) = [1200,350];

%exportgraphics(f,[figure_path,'Fig_GABAr.png'], 'Resolution', 300)





%% What about global complexity?
gLZW_path = '/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/Di_complexity/preprocess_global_LZW/';
% Load parameters. Baseline = first 10 minutes, SWAS = from SWAS to 68min
bl_broad_rho0g = zeros(Ns, 1)*NaN;
swas_broad_rho0g = zeros(Ns, 1)*NaN;
failed = 0;
for ii = 1:Ns
    dynvar = ['subject_', num2str(ii)];
    load([gLZW_path, 'propofol_' subs{ii} '_bench_br_ocICA_avg_prep_global_broad_LZW.mat']);
    Cswas0 = group.(dynvar).Cswas;
    [~, Tswas] = min(abs(fxconc(1:3500) - Cswas0));
    if Cswas0 > 4.5 || ~isreal(Cswas0)
         failed = failed +1;
        continue
    end
    
    % Ask JH about this: why 697???
    bl_broad_rho0g(ii) = mean(rho0_broad(1:120));
    swas_broad_rho0g(ii) = mean(rho0_broad(697:816));
end

% Correlate with each local parameter - which one is most informative?
% Global rho0 vs each channel's local SWAS / LZW parameter
[gr_rhoL_broad_global, gp_rhoL_broad_global] = corr(swas_broad_rho0g, swas_broad_rho0.', 'rows','complete');
[gr_Pswas_broad_global, gp_Pswas_broad_global] = corr(swas_broad_rho0g, Pswas.', 'rows','complete');
[gr_Cswas_broad_global, gp_Cswas_broad_global] = corr(swas_broad_rho0g, Cswas.', 'rows','complete');
[gr_s_broad_global, gp_s_broad_global] = corr(swas_broad_rho0g, s.', 'rows','complete');
[gr_r_broad_global, gp_r_broad_global] = corr(bl_broad_rho0g, r.', 'rows','complete');

%% GLOBAL vs LOCAL correlations figure
f = figure;
subplot(2,3,1)
[p_fdr,p_masked] = fdr(gp_rhoL_broad_global,0.05);
eloc_sig = find(p_masked);
topoplot(gr_rhoL_broad_global,eloc,'maplimits',[-1,1],'emarker',{'.','k',20},'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:30); 
title('Local LZW','fontsize',16);

subplot(2,3,2)
[p_fdr,p_masked] = fdr(gp_s_broad_global,0.05);
eloc_sig = find(p_masked);
topoplot(gr_s_broad_global,eloc,'maplimits',[-1,1],'emarker',{'.','k',20},'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:30); 
title('s','fontsize',16);

subplot(2,3,3)
[p_fdr,p_masked] = fdr(gp_r_broad_global,0.05);
eloc_sig = find(p_masked);
topoplot(gr_r_broad_global,eloc,'maplimits',[-1,1],'emarker',{'.','k',20},'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:30); 
title('r','fontsize',16);

subplot(2,3,4)
[p_fdr,p_masked] = fdr(gp_Pswas_broad_global,0.05, 'nonParametric');
eloc_sig = find(p_masked);
topoplot(gr_Pswas_broad_global,eloc,'maplimits',[-1,1],'emarker',{'.','k',20},'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:30); 
title('P_{SWAS}','fontsize',16);

subplot(2,3,5)
[p_fdr,p_masked] = fdr(gp_Cswas_broad_global,0.05);
eloc_sig = find(p_masked);
topoplot(gr_Cswas_broad_global,eloc,'maplimits',[-1,1],'emarker',{'.','k',20},'emarker2',{eloc_sig,'^','r',5},'plotchans', 1:30); 
title('C_{SWAS}','fontsize',16);

cbar(); set(gca,'fontsize',16);
f.Position(3:4) = [1200,400];
%saveas(gcf,[figure_path,'broad_corr.png'])







%% Slow wave band

bl_slow_rho0 = zeros(32,120,length(subs));
swas_slow_rho0 = zeros(32,120,length(subs));

for ii = 1:length(subs)
    load([lLZW_path, 'propofol_' subs{ii} '_bench_br_ocICA_avg_prep_local_slow_LZW.mat']);
    
    bl_slow_rho0(:,:,ii) = rho0_slow(:,1:120);
    swas_slow_rho0(:,:,ii) = rho0_slow(:,697:816);
end

m_bl_slow_rho0 = mean(bl_slow_rho0,2);
m_bl_slow_rho0 = squeeze(m_bl_slow_rho0);

m_swas_slow_rho0 = mean(swas_slow_rho0,2);
m_swas_slow_rho0 = squeeze(m_swas_slow_rho0);

gm_bl_slow_rho0 = mean(m_bl_slow_rho0,2);
gm_swas_slow_rho0 = mean(m_swas_slow_rho0,2);

for ii = 1:length(m_bl_slow_rho0)
    [gr_r_slow(ii),gp_r(ii)] = corr(m_bl_slow_rho0(ii,:)',r(ii,:)');
    [gr_s_slow(ii),gp_s(ii)] = corr(m_swas_slow_rho0(ii,:)',s(ii,:)');
    [gr_s_min_r_slow(ii),gp_s_min_r_slow(ii)] = corr(m_swas_slow_rho0(ii,:)'-m_bl_slow_rho0(ii,:)',s_min_r(ii,:)');
end

f = figure;
subplot(1,3,1)
topoplot(gr_r_slow,eloc,'maplimits',[-1,1]); title('r','fontsize',16);
subplot(1,3,2)
topoplot(gr_s_slow,eloc,'maplimits',[-1,1]); title('s','fontsize',16);
subplot(1,3,3)
topoplot(gr_s_min_r_slow,eloc,'maplimits',[-1,1]); title('s-r','fontsize',16); cbar()
f.Position(3:4) = [1200,400];
saveas(gcf,[figure_path,'slow_corr.png'])

figure;
g = gramm('x',m_swas_broad_rho0(17,:),'y',s(17,:));
g.geom_point();
g.stat_glm();
g.set_names('x','Local LZW complexity (\rho0)','y','s (dB)');
g.set_text_options('interpreter','tex');
g.draw();
g.export('file_name','Fz_rho0_s','export_path',figure_path,'file_type','png');


% Spatial correlation
[R_r_spat_slow, p_r_spat_slow] = corr(gm_bl_slow_rho0,gm_r);
[R_s_spat_slow,p_s_spat_slow] = corr(gm_swas_slow_rho0,gm_s);
[R_s_min_r_spat_slow, p_s_min_r_spat_slow] = corr(gm_swas_slow_rho0-gm_bl_slow_rho0,gm_s_min_r);


%%
ts = [30.2521
90.7563
151.261
211.765
272.269
332.773
393.277
453.782
514.286
574.79
635.294
695.798
756.303
816.807
877.311
937.815
998.319
1058.82
1119.33
1179.83
1240.34
1300.84
1361.34
1421.85
1482.35
1542.86
1603.36
1663.87
1724.37
1784.87
1845.38
1905.88
1966.39
2026.89
2087.39
2147.9
2208.4
2268.91
2329.41
2389.92
2450.42
2510.92
2571.43
2631.93
2692.44
2752.94
2813.45
2873.95
2934.45
2994.96
3055.46
3115.97
3176.47
3236.97
3297.48
3357.98
3418.49
3478.99
3539.5
3600
3660.5
3721.01
3781.51
3842.02
3902.52
3963.03
4023.53
4084.03
4144.54
4205.04
4265.55
4326.05
4386.55
4447.06
4507.56
4568.07
4628.57
4689.08
4749.58
4810.08
4870.59
4931.09
4991.6
5052.1
5112.61
5173.11
5233.61
5294.12
5354.62
5415.13
5475.63
5536.13
5596.64
5657.14
5717.65
5778.15
5838.66
5899.16
5959.66
6020.17
6080.67
6141.18
6201.68
6262.18
6322.69
6383.19
6443.7
6504.2
6564.71
6625.21
6685.71
6746.22
6806.72
6867.23
6927.73
6988.24
7048.74
7109.24
7169.75]