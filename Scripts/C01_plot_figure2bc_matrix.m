addpath('/Volumes/ExtremeSSD/LZW_Runbei/Scripts');
clear; clc;
%% EEG_LZW
datadir = '/Volumes/Seagate Basic/preprocess_local_LZW/';
subjects = {'001','003','004','005','007','008',...
    '011','013','014','016','017','019',...
    '020','021','022','023'};

%load Channel location file
%addpath('/Volumes/ExtremeSSD/LZW_Runbei/Scripts');
load('/Volumes/ExtremeSSD/Chanlocs.mat');%Ch_Map is the default name
Chanlocs = Ch_Map(1,[1:31]);

% Load TSWAS and LOBR timepoints
load('/Volumes/ExtremeSSD/CleanData/KeyPoints/TSWAS.mat')
load('/Volumes/ExtremeSSD/CleanData/KeyPoints/LOBR.mat')
load('/Volumes/ExtremeSSD/CleanData/KeyPoints/ROBR.mat')
TSWAS = TSWAS+60*10; TSWAS_mk = ceil(TSWAS/5);
LOBR = LOBR+60*10; LOBR_mk = ceil(LOBR/5);
ROBR = ROBR+60*68; ROBR_mk = ceil(ROBR/5);

%% Whole length of LOBR
LZW_All = [];

for ii = 1:length(subjects)
    %prepare for data input
    InPutFileName = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_local_broad_LZW.mat'];
    VarName = ['rho0_broad'];
    %load continuous data
    load([datadir,InPutFileName])
    lzw_whole = eval(VarName);
    lzw = lzw_whole(:,1:1391);
    lzw(1,1392) = lzw(1,1391);
    LZW_All(ii,:,:) = lzw;
end
%% 01_Beginning of the experiment 10-min Pre
LZW_Begin_Onset = LZW_All(:,:,1:120);
%% 02_From Anesthesia Onset to LOBR
LZW_Onset_LOBR = NaN(16,31,max(LOBR_mk)-120+1);
End_LOBR_mk = mean(LOBR_mk)-120;
for ii = 1:16
    LZW_Onset_LOBR(ii,:,1:LOBR_mk(ii)-120) = LZW_All(ii,:,121:LOBR_mk(ii));
end
%% 03_From LOBR to TSWAS
LZW_LOBR_TSWAS = NaN(16,31,max(-LOBR_mk+TSWAS_mk)+1);
End_TSWAS_mk = mean(-LOBR_mk+TSWAS_mk);
for ii = [1:9 12:16]
    LZW_LOBR_TSWAS(ii,:,1:1-LOBR_mk(ii)+TSWAS_mk(ii)) = LZW_All(ii,:,LOBR_mk(ii):TSWAS_mk(ii));
end
%% 04_From TSWAS to The start of the Peak-Anesthesia
TSWAS_mk2 = TSWAS_mk([1:9 12:16],:);
LZW_LOBR_Peak = NaN(16,31,max(696-TSWAS_mk2)+1);
End_Peak_mk = mean(696-TSWAS_mk2);
for ii = [1:9 12:16]
    LZW_LOBR_Peak(ii,:,1:length(TSWAS_mk(ii):696)) = LZW_All(ii,:,TSWAS_mk(ii):696);
end
%% 05_Peak Anesthesia
LZW_Peak_Anes = LZW_All(:,:,697:816);
%% 06_From Peak to ROBR
LZW_Peak_ROBR = NaN(16,31,max(ROBR_mk-816)+1);
End_ROBR_mk = mean(ROBR_mk-816);
for ii = 1:16
    LZW_Peak_ROBR(ii,:,1:length(817:ROBR_mk(ii))) = LZW_All(ii,:,817:ROBR_mk(ii));
end
%% 07_From ROBR to END
LZW_ROBR_End = NaN(16,31,max(1392-ROBR_mk)+1);
End_End_mk = mean(1392-ROBR_mk);
for ii = 1:16
    LZW_ROBR_End(ii,:,1:length(ROBR_mk(ii):1392)) = LZW_All(ii,:,ROBR_mk(ii):1392);
end

%% Topo-Plot
TP_Awake_1 = cal_TP(LZW_Begin_Onset(:,:,1:60)); %first 5 min in Awake
TP_Awake_2 = cal_TP(LZW_Begin_Onset(:,:,61:120)); %second 5 min in Awake
TP_Peak_1 = cal_TP(LZW_Peak_Anes(:,:,1:60));%first 5 min in Peak
TP_Peak_2 = cal_TP(LZW_Peak_Anes(:,:,61:120));%second 5 min in Peak

% +/- 5minutes LOBR
for ii = 1:16
    LZW_LOBR_mk_P5(ii,:,:) = LZW_All(ii,:,LOBR_mk(ii):LOBR_mk(ii)+60-1);
    LZW_LOBR_mk_N5(ii,:,:) = LZW_All(ii,:,LOBR_mk(ii)-60+1:LOBR_mk(ii)); 
end
TP_LOBR_P5 = cal_TP(LZW_LOBR_mk_P5);
TP_LOBR_N5 = cal_TP(LZW_LOBR_mk_N5);


% +/- 5minutes ROBR
for ii = 1:16
    LZW_ROBR_mk_P5(ii,:,:) = LZW_All(ii,:,ROBR_mk(ii):ROBR_mk(ii)+60-1);
    LZW_ROBR_mk_N5(ii,:,:) = LZW_All(ii,:,ROBR_mk(ii)-60+1:ROBR_mk(ii)); 
end
TP_ROBR_P5 = cal_TP(LZW_ROBR_mk_P5);
TP_ROBR_N5 = cal_TP(LZW_ROBR_mk_N5);

% +/- 5minutes SWAS
for ii = 1:16
    LZW_TSWAS_mk_P5(ii,:,:) = LZW_All(ii,:,TSWAS_mk(ii):TSWAS_mk(ii)+60-1);
    LZW_TSWAS_mk_N5(ii,:,:) = LZW_All(ii,:,TSWAS_mk(ii)-60+1:TSWAS_mk(ii)); 
end
TP_TSWAS_P5 = cal_TP(LZW_TSWAS_mk_P5([1:9 12:16],:,:));
TP_TSWAS_N5 = cal_TP(LZW_TSWAS_mk_N5([1:9 12:16],:,:));


label = {'TP_Awake_1','TP_Awake_2','TP_LOBR_N5','TP_LOBR_P5','TP_TSWAS_N5','TP_TSWAS_P5','TP_Peak_1',...
    'TP_Peak_2','TP_ROBR_N5','TP_ROBR_P5'};
label2 = {'Awake1','Awake2','LOBR-5min','LOBR+5min','TSWAS-5min','TSWAS+5min','Peak1','Peak2','ROBR-5min','ROBR+5min'};
for i = 1:length(label)
    for j = 1:length(label)
        corrM(i,j) = corr2(eval(label{i}),eval(label{j}));
    end
end
display_Matrix2(corrM,label2)


TP_1 = cal_TP_tmp(LZW_Begin_Onset(:,:,1:60));%first 5 min in Awake
TP_2 = cal_TP_tmp(LZW_Begin_Onset(:,:,61:120)); %second 5 min in Awake
TP_3 = cal_TP_tmp(LZW_LOBR_mk_N5);
TP_4 = cal_TP_tmp(LZW_LOBR_mk_P5);
TP_5 = cal_TP_tmp(LZW_TSWAS_mk_N5);
TP_6 = cal_TP_tmp(LZW_TSWAS_mk_P5);
TP_7 = cal_TP_tmp(LZW_Peak_Anes(:,:,1:60));
TP_8 = cal_TP_tmp(LZW_Peak_Anes(:,:,61:120));
TP_9 = cal_TP_tmp(LZW_ROBR_mk_N5);
TP_10 = cal_TP_tmp(LZW_ROBR_mk_P5);

TP_All = [];
for k = 1:10
    TP(:,:,k) = eval(['TP_',num2str(k)]);
end

ii = [1:9 12:16];
TP = TP(ii,:,:);


TP_S0 = zeros(1000,14,31,10);
for q = 1:1000
    TP_S1 = zeros(14,31,10);
    for w = 1:14
        TP_S1(w,:,:) = TP(w,:,randperm(10));
    end
    TP_S0(q,:,:,:) = TP_S1;
end

Angle = zeros(1000,10,10);

for q = 1:1000
    angleM = zeros(10,10);
    O = squeeze(TP_S0(q,:,:,:));
    
    for w = 1:10
        for e = 1:10
            A = mean(squeeze(O(:,:,w)),1);
            B = mean(squeeze(O(:,:,e)),1);
            angleM(w,e) = cal_angle(A,B);
            
        end
    end   
    Angle(q,:,:) = angleM;
end


for i = 1:10
    for j = 1:10
        angleM_real(i,j) = cal_angle(eval(label{i}),eval(label{j}));
    end
end

P_corrM = zeros(10,10);
for i = 1:10
    for j = 1:10
        p = 0;
        num = 0;
        for k = 1:1000
            
            if round(Angle(k,i,j),4)> round(angleM_real(i,j),4)
                if i == j
                display(['K = ',num2str(k)])
                display(['I = ',num2str(i)])
                display(['J = ',num2str(j)])
                end
                num = num+1;
            end
        end
        p = num/1000;
        P_corrM(i,j) = p;
    end
end

figure;
display_Matrix2(P_corrM,label2);
figure;
display_Matrix2(angleM_real,label2);
