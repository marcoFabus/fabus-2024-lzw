%% Load Data and transform to eeg in a ' Channels * Time'
clear;clc;
addpath '/Volumes/ExtremeSSD/LZW_Runbei/Scripts';
%% Set Path
datadir = '/Volumes/Seagate Basic/mat';
subjects = {'001', '003', '004', '005',...
            '007', '008', '011', '013',...
            '014', '016', '017', '019',...
            '020', '021', '022','023'};
savedir = '/Volumes/Seagate Basic/preprocess_local/';

% Channel parameters
Chanlocs = [1:31];
Chanlabels = {'Fp1','Fp2','F3','F4','C3','C4',...
    'P3','P4','O1','O2','F7','F8','T7','T8','P7','P8',...
    'Fz','Cz','Pz','Oz','FC1','FC2','CP1','CP2','FC5','FC6',...
    'CP5','CP6','TP9','TP10','POz'};
%% Build Loop and load data
for ii = 1:length(subjects)
    %% Preparation
    filename = ['propofol_',subjects{ii},'_bench_all_ocICA_avg.mat'];
    newfilename = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_local_'];
    %% Initial for each subject
    broad_global = []; % the format would be channels * windows * timepoints
%     slow_global = [];
%     delta_global = [];
%     theta_global = [];
%     alpha_global = [];
%     beta_global = [];
%     gamma_global = [];
    
    for jj = 1:length(Chanlocs)
       
        %display indication
        disp(['Subject: ',subjects{ii}])
        disp('   Loading data...')
        
        %load data and set parameters
        load([datadir,filename]);
        eeg = EEG.data;
        eeg = double(eeg);
        Fs = EEG.srate; %sampling rate
        srate = round(EEG.srate); %sampling rate in integer format
        
        %% Set local electrodes
        chanloc = Chanlocs(jj); % Fz match the number 17
        eeg = eeg(chanloc,:);
        %% Notch and Down Sampling
        % notch fileter
        AC_Fs = 50;      %60 in North American, 50 in Europe
        wo = AC_Fs/(Fs/2);
        Q_Value = 35;    % test number between 1:35
        bw = wo/Q_Value;
        [b,a] = iirnotch(wo,bw);
        eeg = filtfilt(b,a,eeg);
        %% Extracting frequency bands
        disp('   Extracting frequency bands...')
        
        broad = LZW_butterBandpass(eeg,[0.5 90],2,Fs);
%         slow = LZW_butterBandpass(eeg,[0.5 1.5],2,Fs);
%         delta = LZW_butterBandpass(eeg,[1.5 4],2,Fs);
%         theta = LZW_butterBandpass(eeg,[4 8],2,Fs);
%         alpha = LZW_butterBandpass(eeg,[8 14],2,Fs);
%         beta = LZW_butterBandpass(eeg,[14 30],2,Fs);
%         gamma = LZW_butterBandpass(eeg,[30 90],2,Fs);
        %% Windowing data
        disp(' Window sizing data')
        
        l_window = 9;
        l_overlap = 4;
        
        broad_windowed = LZW_Windowing(broad,l_window,l_overlap,srate);
%         slow_windowed = LZW_Windowing(slow,l_window,l_overlap,srate);
%         delta_windowed = LZW_Windowing(delta,l_window,l_overlap,srate);
%         theta_windowed = LZW_Windowing(theta,l_window,l_overlap,srate);
%         alpha_windowed = LZW_Windowing(alpha,l_window,l_overlap,srate);
%         beta_windowed = LZW_Windowing(beta,l_window,l_overlap,srate);
%         gamma_windowed = LZW_Windowing(gamma,l_window,l_overlap,srate);
        
        broad_global = cat(1,broad_global,broad_windowed);
%         slow_global = cat(1,slow_global,slow_windowed);
%         delta_global = cat(1,delta_global,delta_windowed);
%         theta_global = cat(1,theta_global,theta_windowed);
%         alpha_global = cat(1,alpha_global,alpha_windowed);
%         beta_global = cat(1,beta_global,beta_windowed);
%         gamma_global = cat(1,gamma_global,gamma_windowed);
        
    end
    
    save([savedir,newfilename,'broad'],'broad_global');
%     save([savedir,newfilename,'slow'],'slow_global');
%     save([savedir,newfilename,'delta'],'delta_global');
%     save([savedir,newfilename,'theta'],'theta_global');
%     save([savedir,newfilename,'alpha'],'alpha_global');
%     save([savedir,newfilename,'beta'],'beta_global');
%     save([savedir,newfilename,'gamma'],'gamma_global');
end
