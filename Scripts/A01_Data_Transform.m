%% call for EEGLAB
clear all;clc;
eeglab
%% Transform .EEG/.vhdr file to .mat file
eegpath = '/Volumes/Seagate Basic/exp';
savepath = '/Volumes/Seagate Basic/mat';
subjects = {'001', '003', '004', '005', '007', '008', '011', '013', '014', '016', '017', '019', '020', '021', '022','023'};



%% Loop
for ii = 1:length(subjects)
    filevhdr = ['bench_',subjects{ii},'_filt_avg_br_ocICA.vhdr'];
    filesavename = [savepath,'propofol_',subjects{ii},'_bench_all_ocICA_avg.mat'];
    EEG = pop_loadbv(eegpath, filevhdr);
    save(filesavename,'EEG','-mat');
end
