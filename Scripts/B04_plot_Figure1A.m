clear; clc;
%% Raw
Broad = load('/Volumes/Seagate Basic/broad_group.mat');
Shuffle = load('/Volumes/Seagate Basic/shuffle_group.mat');

Broad.Group.DataType(1:21858,:) = {'Real Data'}; 
Shuffle.Group.DataType(1:21858,:) = {'Shuffled Data'}; 

Group.X = [];
Group.Y = [];
Group.Sub = {};
Group.Stage = {};

Group.X = cat(1,Broad.Group.X,Shuffle.Group.X);
Group.Y = cat(1,Broad.Group.Y,Shuffle.Group.Y);
Group.Sub = cat(1,Broad.Group.Sub,Shuffle.Group.Sub);
Group.Stage = cat(1,Broad.Group.Stage,Shuffle.Group.Stage);
Group.DataType = cat(1,Broad.Group.DataType,Shuffle.Group.DataType);

figure
g2 = gramm('x',Group.X,'y',Group.Y,'color',Group.DataType);
g2.facet_grid([],Group.Stage,'scale','free_x','space','free_x');
g2.stat_summary('type','std');
g2.set_order_options('column',{'Rest','Induction_pre_LOBR','Induction_post_LOBR',...
    'Induction_post_TSWAS','Peak','Emergence_pre_ROBR',...
    'Emergence_post_ROBR'});
g2.set_names('column','');
g2.set_names('x','Time(min)','y','Global Complexity');
g2.set_color_options('map','matlab');
g2.set_text_options('font','Arial','base_size',12,'label_scaling',2);
g2.axe_property('YLim',[0.3 1.8],'XTick',[0:60:360],...
    'XTickLabel',{'0','5','10','15','20','25','30'});
g2.draw()

%% Normalized
figure;
clear;clc
Broad = load('/Volumes/Seagate Basic/broad_group.mat');
Shuffle = load('/Volumes/Seagate Basic/shuffle_group.mat');

Broad.Group.DataType(1:21858,:) = {'Real Data'}; 
Shuffle.Group.DataType(1:21858,:) = {'Shuffled Data'}; 
Normal.Group.DataType(1:21858,:) = {'Normalized Data'}; 

Group.X = cat(1,Broad.Group.X,Shuffle.Group.X,Broad.Group.X);
Group.Y = cat(1,Broad.Group.Y,Shuffle.Group.Y,Broad.Group.Y./Shuffle.Group.Y);
Group.Sub = cat(1,Broad.Group.Sub,Shuffle.Group.Sub,Broad.Group.Sub);
Group.Stage = cat(1,Broad.Group.Stage,Shuffle.Group.Stage,Broad.Group.Stage);
Group.DataType = cat(1,Broad.Group.DataType,Shuffle.Group.DataType,Normal.Group.DataType);

figure
g2 = gramm('x',Group.X,'y',Group.Y,'color',Group.DataType);
g2.facet_grid([],Group.Stage,'scale','free_x','space','free_x');
g2.stat_summary('type','std');
g2.set_order_options('column',{'Rest','Induction_pre_LOBR','Induction_post_LOBR',...
    'Induction_post_TSWAS','Peak','Emergence_pre_ROBR',...
    'Emergence_post_ROBR'},'color',{'Real Data','Shuffled Data','Normalized Data'});
g2.set_names('column','');
g2.set_names('x','Time(min)','y','Global Complexity / Normalized Global Complexity');
g2.set_color_options('map','matlab');
g2.set_text_options('font','Arial','base_size',12,'label_scaling',2);
g2.axe_property('YLim',[0 1.8],'XTick',[0:60:360],...
    'XTickLabel',{'0','5','10','15','20','25','30'});
g2.draw()