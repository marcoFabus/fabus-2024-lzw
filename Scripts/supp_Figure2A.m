g(1,2) = gramm('x',Group.DataType,'y',Group.LZW,'color',Group.StageName);
g(1,2).stat_boxplot();
g(1,2).set_names('y','Global Complexity','x',{''},'color',{'Stage'});
g(1,2).set_text_options('font','Arial','base_size',25);
g(1,2).set_title('');
g(1,2).axe_property('YLim',[0 1.2],'YTick',[0:0.2:1.2]);
g(1,2).set_order_options('x',{'EEG','ECG'});
g(1,2).draw();