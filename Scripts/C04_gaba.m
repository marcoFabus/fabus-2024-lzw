clear; clc;
%% EEG_LZW
datadir = '/Volumes/Seagate Basic/preprocess_local_LZW/';
subjects = {'001','003','004','005','007','008',...
    '011','013','014','016','017','019',...
    '020','021','022','023'};
%load Channel location file
%addpath('/Volumes/ExtremeSSD/LZW_Runbei/Scripts');
load('/Volumes/Seagate Basic/Chanlocs.mat');%Ch_Map is the default name
Chanlocs = Ch_Map(1,[1:31]);
% Load TSWAS and LOBR timepoints
load('/Volumes/Seagate Basic/KeyPoints/TSWAS.mat')
load('/Volumes/Seagate Basic/KeyPoints/LOBR.mat')
load('/Volumes/Seagate Basic/KeyPoints/ROBR.mat')
TSWAS = TSWAS+60*10; TSWAS_mk = ceil(TSWAS/5);
LOBR = LOBR+60*10; LOBR_mk = ceil(LOBR/5);
ROBR = ROBR+60*68; ROBR_mk = ceil(ROBR/5);
%% Whole length of LOBR
LZW_All = [];
for ii = 1:length(subjects)
    %prepare for data input
    InPutFileName = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_local_broad_LZW.mat'];
    VarName = ['rho0_broad'];
    %load continuous data
    load([datadir,InPutFileName])
    lzw_whole = eval(VarName);
    lzw = lzw_whole(:,1:1391);
    lzw(1,1392) = lzw(1,1391);
    LZW_All(ii,:,:) = lzw;
end
%% 01_Beginning of the experiment 10-min Pre
LZW_Begin_Onset = LZW_All(:,:,1:120);
%% 02_From Anesthesia Onset to LOBR
LZW_Onset_LOBR = NaN(16,31,max(LOBR_mk)-120+1);
End_LOBR_mk = mean(LOBR_mk)-120;
for ii = 1:16
    LZW_Onset_LOBR(ii,:,1:LOBR_mk(ii)-120) = LZW_All(ii,:,121:LOBR_mk(ii));
end
%% 03_From LOBR to TSWAS
LZW_LOBR_TSWAS = NaN(16,31,max(-LOBR_mk+TSWAS_mk)+1);
End_TSWAS_mk = mean(-LOBR_mk+TSWAS_mk);
for ii = [1:9 12:16]
    LZW_LOBR_TSWAS(ii,:,1:1-LOBR_mk(ii)+TSWAS_mk(ii)) = LZW_All(ii,:,LOBR_mk(ii):TSWAS_mk(ii));
end
%% 04_From TSWAS to The start of the Peak-Anesthesia
TSWAS_mk2 = TSWAS_mk([1:9 12:16],:);
LZW_LOBR_Peak = NaN(16,31,max(696-TSWAS_mk2)+1);
End_Peak_mk = mean(696-TSWAS_mk2);
for ii = [1:9 12:16]
    LZW_LOBR_Peak(ii,:,1:length(TSWAS_mk(ii):696)) = LZW_All(ii,:,TSWAS_mk(ii):696);
end
%% 05_Peak Anesthesia
LZW_Peak_Anes = LZW_All(:,:,697:816);
%% 06_From Peak to ROBR
LZW_Peak_ROBR = NaN(16,31,max(ROBR_mk-816)+1);
End_ROBR_mk = mean(ROBR_mk-816);
for ii = 1:16
    LZW_Peak_ROBR(ii,:,1:length(817:ROBR_mk(ii))) = LZW_All(ii,:,817:ROBR_mk(ii));
end
%% 07_From ROBR to END
LZW_ROBR_End = NaN(16,31,max(1392-ROBR_mk)+1);
End_End_mk = mean(1392-ROBR_mk);
for ii = 1:16
    LZW_ROBR_End(ii,:,1:length(ROBR_mk(ii):1392)) = LZW_All(ii,:,ROBR_mk(ii):1392);
end


label = {'LZW_Begin_Onset','LZW_Onset_LOBR','LZW_LOBR_TSWAS',...
    'LZW_LOBR_Peak','LZW_Peak_Anes','LZW_Peak_ROBR','LZW_ROBR_End'};
label2 = {'Awake','Before LOBR','LOBR to TSWAS',...
    'TSWAS to Peak','Peak Anaesthesia','Peak to ROBR','After ROBR'};

addpath('/Volumes/DZSSD2/withoutbcrejection/GABA/');
load('/Volumes/DZSSD2/withoutbcrejection/GABA/GABA_map.mat');

LZW_local=[];
for i =1:7
    LZW_local(:,i) = eval(['nanmean(nanmean(',label{i},',3),1)'])';
end


for i = 1:7
        [rho(i,1),pvalue(i,1)] = corr(LZW_local(1:31,i),GABA(1:31,1),...
            'Type','Spearman','rows','complete');
end

pvalue_corr =pval_adjust(pvalue,'bonferroni');

Group.LZW = [];
Group.Stage = [];
Group.Gaba = [];

for i = 1:7

        lzw = LZW_local(1:31,i);
        gaba = GABA(1:31,1);
        stage(1:31,1) = i;
        Group.LZW = cat(1,Group.LZW,lzw);
        Group.Stage = cat(1,Group.Stage,stage);
        Group.Gaba = cat(1,Group.Gaba,gaba);

end


g = gramm('x',Group.Gaba,'y',Group.LZW);
g.facet_grid([],Group.Stage);
g.geom_point();
g.stat_glm();
g.set_names('x','GABAA density','y','Local LZW Complexity');

g.draw()

