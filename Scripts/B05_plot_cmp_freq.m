clear;clc;
%% EEG_LZW
datadir = '/Volumes/Seagate Basic/preprocess_global_LZW/';
subjects = {'001','003','004','005','007','008',...
    '011','013','014','016','017','019',...
    '020','021','022','023'};
freqlabel = {'broad','slow','delta','theta','alpha','beta','gamma'};
%freqlabel = {'slow','delta','theta','alpha','beta','gamma'};
%freqlabel = {'broad'};
Group.LZW = [];
Group.Freq = {};
Group.Stage = {};


for jj = 1: length(freqlabel)
    
    LZW_All = [];
    freq = {};
    group.lzw1 = [];
    group.lzw2 = [];
    
    for ii = 1:length(subjects)
        %prepare for data input
        InPutFileName = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_global_',freqlabel{jj},'_LZW.mat'];
        VarName = ['rho0_',freqlabel{jj}];
        %load continuous data
        load([datadir,InPutFileName])
        lzw_whole = eval(VarName);
        lzw = lzw_whole(1,1:1391);
        lzw(1,1392) = lzw(1,1391);
        LZW_All = cat(1,LZW_All,lzw);
    end
    
    % Awake
    EEG_Awake = LZW_All(:,1:120);
    group_lzw1 = mean(EEG_Awake,2);
    Group.LZW = cat(1,Group.LZW,group_lzw1);
    stage(1:length(group_lzw1),1) = {'Awake'};
    Group.Stage = cat(1,Group.Stage,stage);
    % Peak
    EEG_Peak = LZW_All(:,697:816);
    group_lzw2 = mean(EEG_Peak,2);
    Group.LZW = cat(1,Group.LZW,group_lzw2);
    stage(1:length(group_lzw2),1) = {'Peak'};
    Group.Stage = cat(1,Group.Stage,stage);
    
    freq(1:32,1) = freqlabel(jj);
    Group.Freq = cat(1,Group.Freq,freq);
    
end

g(1,2) = gramm('x',Group.Freq,'y',Group.LZW,'color',Group.Stage);
g(1,2).stat_boxplot();
g(1,2).set_names('y','Global Complexity','x',{''},'color',{'Stage'});
g(1,2).set_text_options('font','Arial','base_size',25);
g(1,2).set_title('');
g(1,2).axe_property('YLim',[0 1.2],'YTick',[0:0.2:1.2]);
g(1,2).set_order_options('x',{'broad','slow','delta','theta','alpha','beta','gamma'});

g(1,2).draw();


Broad_Awake = Group.LZW(strcmp(Group.Freq,'broad')&strcmp(Group.Stage,'Awake'));
Broad_Peak = Group.LZW(strcmp(Group.Freq,'broad')&strcmp(Group.Stage,'Peak'));
Slow_Awake = Group.LZW(strcmp(Group.Freq,'slow')&strcmp(Group.Stage,'Awake'));
Slow_Peak = Group.LZW(strcmp(Group.Freq,'slow')&strcmp(Group.Stage,'Peak'));
Delta_Awake = Group.LZW(strcmp(Group.Freq,'delta')&strcmp(Group.Stage,'Awake'));
Delta_Peak = Group.LZW(strcmp(Group.Freq,'delta')&strcmp(Group.Stage,'Peak'));
Theta_Awake = Group.LZW(strcmp(Group.Freq,'theta')&strcmp(Group.Stage,'Awake'));
Theta_Peak = Group.LZW(strcmp(Group.Freq,'theta')&strcmp(Group.Stage,'Peak'));
Alpha_Awake = Group.LZW(strcmp(Group.Freq,'alpha')&strcmp(Group.Stage,'Awake'));
Alpha_Peak = Group.LZW(strcmp(Group.Freq,'alpha')&strcmp(Group.Stage,'Peak'));
Beta_Awake = Group.LZW(strcmp(Group.Freq,'beta')&strcmp(Group.Stage,'Awake'));
Beta_Peak = Group.LZW(strcmp(Group.Freq,'beta')&strcmp(Group.Stage,'Peak'));
Gamma_Awake = Group.LZW(strcmp(Group.Freq,'gamma')&strcmp(Group.Stage,'Awake'));
Gamma_Peak = Group.LZW(strcmp(Group.Freq,'gamma')&strcmp(Group.Stage,'Peak'));

Test = struct();
k = 1;
Test(k,1).type = 'broad';
[Test(k,1).h Test(k,1).p Test(k,1).ci Test(k,1).stats] = ttest(Broad_Awake,Broad_Peak);
k = k+1;

Test(k,1).type = 'slow';
[Test(k,1).h Test(k,1).p Test(k,1).ci Test(k,1).stats] = ttest(Slow_Awake,Slow_Peak);
k = k+1;

Test(k,1).type = 'delta';
[Test(k,1).h Test(k,1).p Test(k,1).ci Test(k,1).stats] = ttest(Delta_Awake,Delta_Peak);
k = k+1;

Test(k,1).type = 'theta';
[Test(k,1).h Test(k,1).p Test(k,1).ci Test(k,1).stats] = ttest(Theta_Awake,Theta_Peak);
k = k+1;

Test(k,1).type = 'alpha';
[Test(k,1).h Test(k,1).p Test(k,1).ci Test(k,1).stats] = ttest(Alpha_Awake,Alpha_Peak);
k = k+1;

Test(k,1).type = 'beta';
[Test(k,1).h Test(k,1).p Test(k,1).ci Test(k,1).stats] = ttest(Beta_Awake,Beta_Peak);
k = k+1;

Test(k,1).type = 'gamma';
[Test(k,1).h Test(k,1).p Test(k,1).ci Test(k,1).stats] = ttest(Gamma_Awake,Gamma_Peak);



%% Relative change
clear;clc;
%% EEG_LZW
datadir = '/Volumes/Seagate Basic/preprocess_global_LZW/';
subjects = {'001','003','004','005','007','008',...
    '011','013','014','016','017','019',...
    '020','021','022','023'};
freqlabel = {'broad','slow','delta','theta','alpha','beta','gamma'};
Group2.LZW = [];
Group2.Freq = {};


for ff = 1: length(freqlabel)
    
    LZW_All = [];
    freq = {};
    group_lzw_awake = [];
    group_lzw_peak = [];
    group_lzw_rc = [];
    
    for ii = 1:length(subjects)
        %prepare for data input
        InPutFileName = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_global_',freqlabel{ff},'_LZW.mat'];
        VarName = ['rho0_',freqlabel{ff}];
        %load continuous data
        load([datadir,InPutFileName])
        lzw_whole = eval(VarName);
        lzw = lzw_whole(1,1:1391);
        lzw(1,1392) = lzw(1,1391);
        LZW_All = cat(1,LZW_All,lzw);
    end
    
    % Awake
    EEG_Awake = LZW_All(:,1:120);
    group_lzw_awake = mean(EEG_Awake,2);

    % Peak
    EEG_Peak = LZW_All(:,697:816);
    group_lzw_peak = mean(EEG_Peak,2);
    
    % Calculate Relative Change
    group_lzw_rc = (group_lzw_awake - group_lzw_peak)./group_lzw_awake;
    group_lzw_rc = group_lzw_rc .* 100;
    
    Group2.LZW = cat(1,Group2.LZW,group_lzw_rc);

    
    freq(1:16,1) = freqlabel(ff);
    Group2.Freq = cat(1,Group2.Freq,freq);
    
end

g(1,3) = gramm('x',Group2.Freq,'y',Group2.LZW);
g(1,3).stat_boxplot();
g(1,3).set_names('y','Relative Change (%)','x',{''});
g(1,3).set_text_options('font','Arial','base_size',25);
g(1,3).set_title('');
%g(1,3).axe_property('YLim',[0 100],'YTick',[0:10:100]);
g(1,3).set_color_options('map','brewer_paired');
g(1,3).set_order_options('x',{'broad','slow','delta','theta','alpha','beta','gamma'});
g(1,3).draw();

g(1,3) = gramm('x',Group2.Freq,'y',Group2.LZW,'subset',~strcmp(Group2.Freq,'broad'));
g(1,3).stat_boxplot();
g(1,3).set_names('y','Relative Change (%)','x',{''});
g(1,3).set_text_options('font','Arial','base_size',25);
g(1,3).set_title('');
%g(1,3).axe_property('YLim',[0 100],'YTick',[0:10:100]);
g(1,3).set_color_options('map','brewer_paired');
g(1,3).set_order_options('x',{'broad','slow','delta','theta','alpha','beta','gamma'});
g(1,3).draw();

%% To GraphPad for statistics
cat(1,Broad_Awake,Slow_Awake,Delta_Awake,Theta_Awake,Alpha_Awake,Beta_Awake,Gamma_Awake);
cat(1,Broad_Peak,Slow_Peak,Delta_Peak,Theta_Peak,Alpha_Peak,Beta_Peak,Gamma_Peak);