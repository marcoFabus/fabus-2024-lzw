clear; clc;
addpath '/Volumes/ExtremeSSD/LZW_Runbei/Scripts';
%% EEG_LZW
datadir = '/Volumes/Seagate Basic/preprocess_local_LZW/';
subjects = {'001','003','004','005','007','008',...
    '011','013','014','016','017','019',...
    '020','021','022','023'};
%load Channel location file
load('/Volumes/ExtremeSSD/Chanlocs.mat');%Ch_Map is the default name
Chanlocs = Ch_Map(1,[1:31]);
% Load TSWAS and LOBR timepoints
load('/Volumes/ExtremeSSD/CleanData/KeyPoints/TSWAS.mat')
load('/Volumes/ExtremeSSD/CleanData/KeyPoints/LOBR.mat')
load('/Volumes/ExtremeSSD/CleanData/KeyPoints/ROBR.mat')
TSWAS = TSWAS+60*10; TSWAS_mk = ceil(TSWAS/5);
LOBR = LOBR+60*10; LOBR_mk = ceil(LOBR/5);
ROBR = ROBR+60*68; ROBR_mk = ceil(ROBR/5);
%% Whole length of LZW
LZW_All = [];
for ii = 1:length(subjects)
    %prepare for data input
    InPutFileName = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_local_broad_LZW.mat'];
    VarName = ['rho0_broad'];
    %load continuous data
    load([datadir,InPutFileName])
    lzw_whole = eval(VarName);
    lzw = lzw_whole(:,1:1391);
    lzw(1,1392) = lzw(1,1391);
    LZW_All(ii,:,:) = lzw;
end
%% Creat LZW_feature
LZW_feature = [];
for ii = 1:16
    LZW = squeeze(LZW_All(ii,:,:));
    LZW_feature = cat(1,LZW_feature,LZW');
end
%% Kmeans Clustering
rng('default')
n = 4;
% n = 3;
% n = 4;
% n = 5;
[idx,C,sumd,D] = kmeans(LZW_feature,n,'MaxIter',10000,...
    'Display','final','Replicates',50);
% See the zscore limits of the for clusters
% for ni = 1:n
%     max(zscore(C(ni,1:31)))
%     min(zscore(C(ni,1:31)))
% end

figure
for ni = 1:n
    subplot(1,n,ni)
    topoplot(zscore(C(ni,1:31)),Chanlocs,'electrodes','on','maplimits',[-2.5,2.5],'colormap',turbo(32));
    LZW_topoplot_setpos(1.2);
end


%% Elbow Method
X = LZW_feature;
dim=size(X);
% default number of test to get minimun under differnent random centriods
test_num=10;
distortion=zeros(10,1);
for k_temp=1:10
    [~,~,sumd]=kmeans(X,k_temp,'emptyaction','drop');
    destortion_temp=sum(sumd);
    % try differnet tests to find minimun disortion under k_temp clusters
    for test_count=2:test_num
        [~,~,sumd]=kmeans(X,k_temp,'emptyaction','drop');
        destortion_temp=min(destortion_temp,sum(sumd));
    end
    distortion(k_temp,1)=destortion_temp;
end

variance=distortion(1:end-1)-distortion(2:end);
distortion_percent=cumsum(variance)/(distortion(1)-distortion(end));
plot(distortion_percent,'b*--');
g_d = gramm('x',[1:10],'y',distortion);
g_d.geom_point()
g_d.geom_line()
g_d.geom_vline('xintercept',4,'style','k--');
g_d.set_title('Elbow Method')
g_d.set_names('x','Numbers of Clusters','y',{'Distortion'});
g_d.set_text_options('base_size',25);
g_d.draw()
%% Plot the distribution of Clusters
figure
clear g
Group2.Sub = [];
Group2.Stage = [];
Group2.Cluster = [];
idx_Sub = reshape(idx,1392,16);
idx_Sub = idx_Sub';
for ii = [1:9 12:16]
    Stage =[];
    cluster = [];
    sub = [];
    cluster = idx_Sub(ii,:)';
    Group2.Cluster = cat(1,Group2.Cluster,cluster);
    sub = ones(length(cluster),1).*ii;
    Group2.Sub = cat(1,Group2.Sub,sub);
    stage1 = 1:LOBR_mk(ii)-1;
    stage2 = LOBR_mk(ii):TSWAS_mk(ii)-1;
    stage3 = TSWAS_mk(ii):696
    stage4 = 697:816;
    stage5 = 817:ROBR_mk(ii)-1;
    stage6 = ROBR_mk(ii):1392;
    Stage(stage1,1)=1;
    Stage(stage2,1)=2;
    Stage(stage3,1)=3;
    Stage(stage4,1)=4;
    Stage(stage5,1)=5;
    Stage(stage6,1)=6;
    Group2.Stage = cat(1,Group2.Stage,Stage);
end
g=gramm('x',Group2.Stage,'color',Group2.Cluster);
g.stat_bin('normalization','probability','width',3,'dodge',3);
g.set_names('x','','color',{'Cluster'},'y','Probability');
g.axe_property('XTick',[1:6],'XTickLabel',{'Before LOBR','LOBR-TSWAS','TSWAS-Peak','Peak','Peak-ROBR','After ROBR'});
g.set_text_options('base_size',25);
g.draw();

%% Plot the distribution of Clusters for 7 Stage
figure
clear g
Group2.Sub = [];
Group2.Stage = [];
Group2.Cluster = [];
idx_Sub = reshape(idx,1392,16);
idx_Sub = idx_Sub';
for ii = [1:9 12:16]
    Stage =[];
    cluster = [];
    sub = [];
    cluster = idx_Sub(ii,:)';
    Group2.Cluster = cat(1,Group2.Cluster,cluster);
    sub = ones(length(cluster),1).*ii;
    Group2.Sub = cat(1,Group2.Sub,sub);
    stage1 = 1:120;
    stage2 = 121:LOBR_mk(ii)-1;
    stage3 = LOBR_mk(ii):TSWAS_mk(ii)-1;
    stage4 = TSWAS_mk(ii):696
    stage5 = 697:816;
    stage6 = 817:ROBR_mk(ii)-1;
    stage7 = ROBR_mk(ii):1392;
    Stage(stage1,1)=1;
    Stage(stage2,1)=2;
    Stage(stage3,1)=3;
    Stage(stage4,1)=4;
    Stage(stage5,1)=5;
    Stage(stage6,1)=6;
    Stage(stage7,1)=7;
    Group2.Stage = cat(1,Group2.Stage,Stage);
end
g=gramm('x',Group2.Stage,'color',Group2.Cluster);
g.stat_bin('normalization','probability','width',3,'dodge',3);
g.set_names('x','','color',{'Cluster'},'y','Probability');
g.axe_property('XTick',[1:7],'XTickLabel',...
    {'Stage I','Stage II','Stage III','Stage IV','Stage V','Stage VI','Stage VII'});
g.set_text_options('base_size',25);
g.draw();