%% Generate continue_whole_length_ocICA_LZW_all
addpath('/Volumes/Seagate Basic/Scripts_func');
clear;clc;
datadir = '/Volumes/Seagate Basic/preprocess_global_LZW/';
subjects = {'001','003','004','005','007','008',...
    '011','013','014','016','017','019',...
    '020','021','022','023'};
LZW_All = [];

for ii = 1:length(subjects)
    %prepare for data input
    InPutFileName = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_global_broad_LZW.mat'];
    VarName = ['rho0_broad'];
    %load continuous data
    load([datadir,InPutFileName])
    lzw_whole = eval(VarName);
    lzw = lzw_whole(1,1:1391);
    lzw(1,1392) = lzw_whole(1,1391);
    LZW_All = cat(1,LZW_All,lzw);
end

% Load TSWAS and LOBR timepoints
load('/Volumes/Seagate Basic/KeyPoints/TSWAS.mat')
load('/Volumes/Seagate Basic/KeyPoints/LOBR.mat')
load('/Volumes/Seagate Basic/KeyPoints/ROBR.mat')
TSWAS = TSWAS+60*10;
LOBR = LOBR+60*10;
ROBR = ROBR+60*68;
% Load LZW_All  (VarName = LZW_All)
%%%%%load('/Volumes/ExtremeSSD/ReStartProject/continue_wholelength_LZW_all.mat')
%% Beginning of the experiment 10-min Pre
g(1,1) = gramm('x',[1:120],'y',LZW_All(:,1:120))
g(1,1).stat_summary('type','std');
g(1,1).axe_property('XLim',[0 120],'XTick',[0:60:120],'XTickLabel',{'0','5','10'});
%% From Anesthesia Onset to LOBR
LOBR_mk = ceil(LOBR/5);
LZW_Onset_LOBR = NaN(16,max(LOBR_mk)-120);
End_LOBR_mk = mean(LOBR_mk)-120;
for ii = 1:16
    LZW_Onset_LOBR(ii,1:LOBR_mk(ii)-120) = LZW_All(ii,121:LOBR_mk(ii));
end
g(1,2) = gramm('x',[1:End_LOBR_mk],'y',LZW_Onset_LOBR(:,1:End_LOBR_mk));
g(1,2).stat_summary('type','std');
g(1,2).axe_property('XLim',[0 240],'XTick',[0:60:240],...
    'XTickLabel',{'0','5','10','15','20'});
%% From LOBR to TSWAS
TSWAS_mk = ceil(TSWAS/5);
LZW_LOBR_TSWAS = NaN(16,max(-LOBR_mk+TSWAS_mk));
End_TSWAS_mk = mean(-LOBR_mk+TSWAS_mk);
for ii = [1:9 12:16]
    LZW_LOBR_TSWAS(ii,1:1-LOBR_mk(ii)+TSWAS_mk(ii)) = LZW_All(ii,LOBR_mk(ii):TSWAS_mk(ii));
end
g(1,3) = gramm('x',[1:End_TSWAS_mk],'y',LZW_LOBR_TSWAS(:,1:End_TSWAS_mk));
g(1,3).stat_summary('type','std');
g(1,3).axe_property('XLim',[0 180],'XTick',[0:60:180],...
    'XTickLabel',{'0','5','10','15'});
%% From TSWAS to The start of the Peak-Anesthesia
TSWAS_mk2 = TSWAS_mk([1:9 12:16],:);
LZW_LOBR_Peak = NaN(16,max(696-TSWAS_mk2));
End_Peak_mk = mean(696-TSWAS_mk2);
for ii = [1:9 12:16]
    LZW_LOBR_Peak(ii,1:length(TSWAS_mk(ii):696)) = LZW_All(ii,TSWAS_mk(ii):696);
end
g(1,4) = gramm('x',[1:End_Peak_mk],'y',LZW_LOBR_Peak(:,1:End_Peak_mk));
g(1,4).stat_summary('type','std');
g(1,4).axe_property('XLim',[0 180],'XTick',[0:60:180],...
    'XTickLabel',{'0','5','10','15'});
%% Peak Anesthesia
g(1,5) = gramm('x',[1:120],'y',LZW_All(:,697:816))
g(1,5).stat_summary('type','std');
g(1,5).axe_property('XLim',[0 120],'XTick',[0:60:120],'XTickLabel',{'0','5','10'});
%% From Peak to ROBR
ROBR_mk = ceil(ROBR/5);
LZW_Peak_ROBR = NaN(16,max(ROBR_mk-816));
End_ROBR_mk = mean(ROBR_mk-816);
for ii = 1:16
    LZW_Peak_ROBR(ii,1:length(817:ROBR_mk(ii))) = LZW_All(ii,817:ROBR_mk(ii));
end
g(1,6) = gramm('x',[1:End_ROBR_mk],'y',LZW_Peak_ROBR(:,1:End_ROBR_mk));
g(1,6).stat_summary('type','std');
g(1,6).axe_property('XLim',[0 240],'XTick',[0:60:240],...
    'XTickLabel',{'0','5','10','15','20'});
%% From ROBR to END
LZW_ROBR_End = NaN(16,max(1392-ROBR_mk));
End_End_mk = mean(1392-ROBR_mk);
for ii = 1:16
    LZW_ROBR_End(ii,1:length(ROBR_mk(ii):1392)) = LZW_All(ii,ROBR_mk(ii):1392);
end
g(1,7) = gramm('x',[1:End_End_mk],'y',LZW_ROBR_End(:,1:End_End_mk));
g(1,7).stat_summary('type','std');
g(1,7).axe_property('XLim',[0 300],'XTick',[0:60:240],...
    'XTickLabel',{'0','5','10','15','20','25'});

%% Real draw
g.set_color_options('map','d3_20');
g.axe_property('YLim',[0.4 1.0]);
g.draw()


LZW_Begin_Onset = LZW_All(:,1:120);
LZW_Peak = LZW_All(:,697:816);

%% Plot Final Figure
Label = {'LZW_Begin_Onset','LZW_Onset_LOBR','LZW_LOBR_TSWAS',...
    'LZW_LOBR_Peak','LZW_Peak','LZW_Peak_ROBR','LZW_ROBR_End'};
Stage = {'Rest','Induction_pre_LOBR','Induction_post_LOBR',...
    'Induction_post_TSWAS','Peak','Emergence_pre_ROBR',...
    'Emergence_post_ROBR'};
subjects = {'001','003','004','005','007',...
    '008','011','013','014','016','017','019',...
    '020','021','022','023'};
marker = [120,223,193,182,120,232,343];
Group.X = [];
Group.Y = [];
Group.Sub = {};
Group.Stage = {};

for j = 1:length(Label)
    
    if j == 3 | j ==  4
        for i = [1:9 12:16]
            lzw_y = eval(Label{j});
            lzw_y = lzw_y(:,1:marker(j));
            sub = {};
            stage = {};
            Group.Y = cat(1,Group.Y,lzw_y(i,:)');
            Group.X = cat(1,Group.X,[1:length(lzw_y(i,:))]');
            sub(1:length(lzw_y(i,:))) = {subjects{i}};
            Group.Sub = cat(1,Group.Sub,sub');
            stage(1:length(lzw_y(i,:))) = {Stage{j}};
            Group.Stage = cat(1,Group.Stage,stage');
        end
        continue
    end

    
    for i = 1:16
        lzw_y = eval(Label{j});
        lzw_y = lzw_y(:,1:marker(j));
        sub = {};
        stage = {};
        Group.Y = cat(1,Group.Y,lzw_y(i,:)');
        Group.X = cat(1,Group.X,[1:length(lzw_y(i,:))]');
        sub(1:length(lzw_y(i,:))) = {subjects{i}};
        Group.Sub = cat(1,Group.Sub,sub');
        stage(1:length(lzw_y(i,:))) = {Stage{j}};
        Group.Stage = cat(1,Group.Stage,stage');
    end
    
end

figure
g2 = gramm('x',Group.X,'y',Group.Y);
g2.facet_grid([],Group.Stage,'scale','free_x','space','free_x');
g2.stat_summary('type','std');
g2.set_order_options('column',{'Rest','Induction_pre_LOBR','Induction_post_LOBR',...
    'Induction_post_TSWAS','Peak','Emergence_pre_ROBR',...
    'Emergence_post_ROBR'});
g2.set_names('column','');
g2.set_names('x','Time(min)','y','Global Complexity');
g2.set_color_options('map','matlab');
g2.set_text_options('font','Arial','base_size',12,'label_scaling',2);
g2.axe_property('YLim',[0.4 1.2],'XTick',[0:60:360],...
    'XTickLabel',{'0','5','10','15','20','25','30'});
g2.draw()
% Save the variable Group and named it as "broad_group.mat"

%% plot average in each stage
Label = {'LZW_Begin_Onset','LZW_Onset_LOBR','LZW_LOBR_TSWAS',...
    'LZW_LOBR_Peak','LZW_Peak','LZW_Peak_ROBR','LZW_ROBR_End'};
Stage = {'Rest','Induction_pre_LOBR','Induction_post_LOBR',...
    'Induction_post_TSWAS','Peak','Emergence_pre_ROBR',...
    'Emergence_post_ROBR'};
subjects = {'001','003','004','005','007',...
    '008','011','013','014','016','017','019',...
    '020','021','022','023'};
marker = [120,223,193,182,120,232,343];
Group.X = [];
Group.Y = [];
Group.Sub = {};
Group.Stage = {};

for j = 1:length(Label)
    
    if j == 3| j ==  4
        for i = [1:9 12:16]
            lzw_y = eval(Label{j});
            lzw_y = nanmean(lzw_y(:,1:marker(j)),2);
            sub = {};
            stage = {};
            Group.Y = cat(1,Group.Y,lzw_y(i,:)');
            Group.X = cat(1,Group.X,[1:length(lzw_y(i,:))]');
            sub(1:length(lzw_y(i,:))) = {subjects{i}};
            Group.Sub = cat(1,Group.Sub,sub');
            stage(1:length(lzw_y(i,:))) = {Stage{j}};
            Group.Stage = cat(1,Group.Stage,stage');
        end
        continue
    end

    
    for i = 1:16
        lzw_y = eval(Label{j});
        lzw_y = nanmean(lzw_y(:,1:marker(j)),2);
        sub = {};
        stage = {};
        Group.Y = cat(1,Group.Y,lzw_y(i,:)');
        Group.X = cat(1,Group.X,[1:length(lzw_y(i,:))]');
        sub(1:length(lzw_y(i,:))) = {subjects{i}};
        Group.Sub = cat(1,Group.Sub,sub');
        stage(1:length(lzw_y(i,:))) = {Stage{j}};
        Group.Stage = cat(1,Group.Stage,stage');
    end
    
end
figure
g2 = gramm('x',Group.Stage,'y',Group.Y);
g2.facet_grid([],Group.Stage,'scale','free_x','space','free_x');
g2.stat_boxplot();
g2.set_order_options('column',{'Rest','Induction_pre_LOBR','Induction_post_LOBR',...
    'Induction_post_TSWAS','Peak','Emergence_pre_ROBR',...
    'Emergence_post_ROBR'});
g2.set_names('column','');
g2.set_names('x','','y','Global Complexity');
g2.set_color_options('map','matlab');
g2.set_text_options('font','Arial','base_size',12,'label_scaling',2);
g2.draw()

%% Go to GraphPad for statistics
Table = [];

for j = 1:length(Label)
    t = NaN(16,1);
    if j == 3| j ==  4
        t([1:9 12:16]) = Group.Y(strcmp(Group.Stage,Stage{j}));
    else
        t = Group.Y(strcmp(Group.Stage,Stage{j}));
    end
    Table = cat(2,Table,t);
end
