function Pos = LZW_topoplot_setpos(set_pos)
    ax = get(gca);
    ax.Position(1,3) = set_pos*ax.Position(1,3);
    ax.Position(1,4) = set_pos*ax.Position(1,4);
    Pos = set(gca,'position',ax.Position)
end