function windowed_data = LZW_Windowing(data,l_window,l_overlap,Fs)
	%data = nxm (n number of eeg channels and m number of data per channel)
	%l_window = window size in seconds
	%l_overlap = window overlap size in seconds
	%Fs = smapling rate
    n_windows = (size(data,2)-l_overlap*Fs)/((l_window-l_overlap)*Fs);%number of windows extracted from data
    %% cut data if the last bit doesn't fit into a full window
    if floor(n_windows) ~= n_windows
        warning('Input not evently divisible with the window size. Remainder has been truncated')
        data = data(:,1:(end-rem((size(data,2)-l_overlap*Fs),((l_window-l_overlap)*Fs))));
        n_windows = floor(n_windows);
    end

    %% create a (channel number)x(window number)x(number of data points per window) matrix as an output 
    windowed_data = zeros(size(data,1),n_windows,l_window*Fs);

    for channelnum = 1:size(data,1)
        for windownum = 1:n_windows
            windowed_data(channelnum,windownum,:) = data(channelnum,(1+(windownum-1)...
                *(l_window-l_overlap)*Fs):(l_window*Fs+(windownum-1)...
                *(l_window-l_overlap)*Fs));
        end
    end
end



