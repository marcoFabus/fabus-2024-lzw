function angle = cal_angle(A,B)

angle = dot(A,B)/(norm(A)*norm(B));


end