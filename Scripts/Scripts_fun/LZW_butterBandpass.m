function filteeg = LZW_butterBandpass(data, freqs, order, Fs)

[BB, AA] = butter(order,freqs/(Fs/2),'bandpass');
filteeg = zeros(size(data));

for ii = 1:size(data,1)
    filteeg(ii,:) = filtfilt(BB,AA,data(ii,:));
end