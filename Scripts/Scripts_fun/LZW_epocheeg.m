function epochedeeg = LZW_epocheeg(data,l_epoch,Fs)

n_epochs = size(data,2)/(l_epoch*Fs);

if floor(n_epochs) ~= n_epochs
    warning('Input not evently divisible with the epoch length. Remainder has been truncated')
    data = data(:,1:floor(n_epochs)*l_epoch*Fs);
end

epochedeeg = zeros(size(data,1),size(data,2)/(l_epoch*Fs),l_epoch*Fs);

for chan = 1:size(data,1)
    eeg = data(chan,1:end);
    epoched_chan = reshape(eeg,Fs*l_epoch,[]).';
    epochedeeg(chan,:,:) = epoched_chan;
end