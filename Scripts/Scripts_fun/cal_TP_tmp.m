function TP = cal_TP_tmp(LZW_All)


    
    TP = [];
    
    All = nanmean(LZW_All,3);
    
    for kk = 1:16
        Z_All(:,kk) = zscore(All(kk,:));
    end
    
    TP = Z_All';



end