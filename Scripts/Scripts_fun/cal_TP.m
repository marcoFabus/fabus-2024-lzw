function TP = cal_TP(LZW_All)


    
    TP = [];
    
    All = nanmean(LZW_All,3);
    
    [s a b] = size(LZW_All); 
    
    for kk = 1:s
        Z_All(:,kk) = zscore(All(kk,:));
    end
    
    TP = nanmean(Z_All,2);



end