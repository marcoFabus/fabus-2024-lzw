function bin_eeg = LZW_Bin(data)
m = mean(data);
for i = 1:length(data)
    if data(1,i) < m
        data(1,i) = 0;
    else
        data(1,i) = 1;
    end
end
bin_eeg = data;
end