function display_Matrix(corrM,label2)
ll = 1:length(label2)
imagesc(corrM);            % Create a colored plot of the matrix values
colormap(flipud(gray));  % Change the colormap to gray (so higher values are
%   black and lower values are white)

textStrings = num2str(corrM(:), '%0.2f');       % Create strings from the matrix values
textStrings = strtrim(cellstr(textStrings));  % Remove any space padding
[x, y] = meshgrid(ll);  % Create x and y coordinates for the strings
hStrings = text(x(:), y(:), textStrings(:), ...  % Plot the strings
    'HorizontalAlignment', 'center');
midValue = mean(get(gca, 'CLim'));  % Get the middle value of the color range
textColors = repmat(corrM(:) > midValue, 1, 3);  % Choose white or black for the
%   text color of the strings so
%   they can be easily seen over
%   the background color
set(hStrings, {'Color'}, num2cell(textColors, 2));  % Change the text colors

set(gca, 'XTick', ll, ...                             % Change the axes tick marks
    'XTickLabel', label2, ...  %   and tick labels
    'YTick', ll, ...
    'YTickLabel', label2, ...
    'TickLength', [0 0],...
    'FontSize',15);
end