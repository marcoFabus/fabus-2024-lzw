function Emg_tp = LZW_cmptp(timepoints)
subjects = {'001','003','004','005','007','008','011','013','014','016','017','019','020','021','022','023'};
freqlabel = {'broad'};
datadir_2 = '/Volumes/ExtremeSSD/ReStartProject/CleanData_gen_Ch/Emergence_LZW/';
for jj = 1:length(freqlabel)
    Emg_All = zeros(32,16);
    for ii = 1:length(subjects)
        Post = load([datadir_2,'propofol_',...
            subjects{ii},'_bench_avg_emg_Ch_',freqlabel{jj},...
            '_LZW.mat']); % Pre means LOBR happened before ROBR
        LZW = eval(['Post.rho0_',freqlabel{jj}]);
        Emg_All(:,ii) = LZW(:,timepoints(ii));
    end
end


for ii = 1:length(subjects)
    Emg_All(:,ii) = zscore(Emg_All(:,ii));
end
Emg_tp = mean(Emg_All,2);
end