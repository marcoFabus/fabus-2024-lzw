function Ind_tp = LZW_ind_cmptp(timepoints)
subjects = {'001','003','004','005','007','008','011','013','014','016','017','019','020','021','022','023'};
freqlabel = {'broad'};
datadir_1 = '/Volumes/ExtremeSSD/ReStartProject/CleanData_gen_Ch/Induction_LZW/';
for jj = 1:length(freqlabel)
    Ind_All = zeros(32,16);
    for ii = 1:length(subjects)
        Pre = load([datadir_1,'propofol_',...
            subjects{ii},'_bench_avg_ind_Ch_',freqlabel{jj},...
            '_LZW.mat']); % Pre means LOBR happened before ROBR
        LZW = eval(['Pre.rho0_',freqlabel{jj}]);
        Ind_All(:,ii) = LZW(:,timepoints(ii));
    end
end


for ii = 1:length(subjects)
    Ind_All(:,ii) = zscore(Ind_All(:,ii));
end
Ind_tp = mean(Ind_All,2);
end