% This is a script I used to generate global data in Induction Process
% This script is used to illustrate the method for generating global data
% Awake & Peak & Induction will use same method and same parameters
%% Load Data
% These data are preprocessed by BrainVision Analyzer(ocular ICA)
% and transformed to .mat by EEGLAB's 'pop_loadbv' function
addpath '/Volumes/ExtremeSSD/LZW_Runbei/Scripts';
clear;clc;
%% Set Path
datadir = '/Volumes/Seagate Basic/mat/';
subjects = {'001', '003', '004', '005',...
            '007', '008', '011', '013',...
            '014', '016', '017', '019',...
            '020', '021', '022','023'};
savedir = '/Volumes/Seagate Basic/preprocess_global_EKG/';

% Channel parameters
Chanlocs = [32 33];
Chanlabels = {'ECG','ECG2'};

%% Build Loop and load data
for ii = 1:length(subjects)
    %% Preparation
    filename = ['propofol_',subjects{ii},'_bench_all_ocICA_avg.mat'];
    newfilename = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_global_'];
    %% Initial global var for each subject in each frequency band
    broad_global = [];
    slow_global = [];
    delta_global = [];
    theta_global = [];
    alpha_global = [];
    beta_global = [];
    gamma_global = [];
    
    for jj = 1:length(Chanlocs)
        %display indication
        disp(['Subject: ',subjects{ii}])
        disp('   Loading data...')
        
        %load data and set parameters
        load([datadir,filename]);
        eeg = EEG.data;
        eeg = double(eeg);
        Fs = EEG.srate; %sampling rate
        srate = round(EEG.srate); %sampling rate in integer format
        %% Set local electrodes
        chanloc = Chanlocs(jj); % eg. Fz match the number 17
        eeg = eeg(chanloc,:); %select one channel data
        %% Notch filter
        % notch filter
        AC_Fs = 50;%60 in North American, 50 in Europe
        wo = AC_Fs/(Fs/2);
        Q_Value = 35; % test number between 1:35
        bw = wo/Q_Value;
        [b,a] = iirnotch(wo,bw);
        eeg = filtfilt(b,a,eeg);
        
        %% Extracting frequency bands
        disp('   Extracting frequency bands...')
        broad = LZW_butterBandpass(eeg,[0.5 90],2,Fs);
        slow = LZW_butterBandpass(eeg,[0.5 1.5],2,Fs);
        delta = LZW_butterBandpass(eeg,[1.5 4],2,Fs);
        theta = LZW_butterBandpass(eeg,[4 8],2,Fs);
        alpha = LZW_butterBandpass(eeg,[8 14],2,Fs);
        beta = LZW_butterBandpass(eeg,[14 30],2,Fs);
        gamma = LZW_butterBandpass(eeg,[30 90],2,Fs);
        %% Windowing data
        disp(' Window sizing data')
        %set parameters of the windowing function
        %we decided to use 9s for window-length and 4s for overlap-length
        l_window = 9;
        l_overlap = 4;
        
        % Windowing data by the customized function 'LZW_Windowing'
        
        broad_windowed = LZW_Windowing(broad,l_window,l_overlap,srate);
        slow_windowed = LZW_Windowing(slow,l_window,l_overlap,srate);
        delta_windowed = LZW_Windowing(delta,l_window,l_overlap,srate);
        theta_windowed = LZW_Windowing(theta,l_window,l_overlap,srate);
        alpha_windowed = LZW_Windowing(alpha,l_window,l_overlap,srate);
        beta_windowed = LZW_Windowing(beta,l_window,l_overlap,srate);
        gamma_windowed = LZW_Windowing(gamma,l_window,l_overlap,srate);
        
        %% Concatenate all channels to global data
        
        broad_global = cat(3,broad_global,broad_windowed);
        slow_global = cat(3,slow_global,slow_windowed);
        delta_global = cat(3,delta_global,delta_windowed);
        theta_global = cat(3,theta_global,theta_windowed);
        alpha_global = cat(3,alpha_global,alpha_windowed);
        beta_global = cat(3,beta_global,beta_windowed);
        gamma_global = cat(3,gamma_global,gamma_windowed);
        
    end
    %% Save Output
    save([savedir,newfilename,'broad'],'broad_global');
    save([savedir,newfilename,'slow'],'slow_global');
    save([savedir,newfilename,'delta'],'delta_global');
    save([savedir,newfilename,'theta'],'theta_global');
    save([savedir,newfilename,'alpha'],'alpha_global');
    save([savedir,newfilename,'beta'],'beta_global');
    save([savedir,newfilename,'gamma'],'gamma_global');
    
end
