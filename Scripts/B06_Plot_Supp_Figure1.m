%% Induction---LZW changes follow fxconc
%% Initializing
clear;clc
%% Set Path
datadir = '/Volumes/Seagate Basic/preprocess_global_LZW/';
subjects = {'001','003','004','005','007','008','011','013','014','016','017','019','020','021','022','023'};
freqlabel = {'broad'};

% Import Data of Drug Concentration
anespath = '/Volumes/ExtremeSSD/LZW_Runbei/Data/Drug/';
fxconcname = 'propofol_unaligned_smoothed.mat';
load([anespath,fxconcname]); % NOTE: the variable name is : fxconc
%
fxconc = cat(2,zeros(1,600),fxconc,zeros(1,49));
 
 %Initial Var about Goodness of Fit
 R2_all = zeros(16,8); % Var for adjusted R-square
 RMSE_all = zeros(16,8);% Var for root-mean-square error 
 
 % Load TSWAS and LOBR timepoints
 load('/Volumes/ExtremeSSD/CleanData/KeyPoints/TSWAS.mat')
 load('/Volumes/ExtremeSSD/CleanData/KeyPoints/LOBR.mat')
 load('/Volumes/ExtremeSSD/CleanData/KeyPoints/ROBR.mat')
 TSWAS = TSWAS+600;
 LOBR = LOBR+600;
 ROBR = ROBR+60*68;
 
 % All subjects LZW across timepoints
 LZW_All = [];
for jj = 1:length(freqlabel)
    figure1 = figure;
    OutPutFileName = ['propofol_',freqlabel{jj},'_Gb_pre_ind_post.png'];
    OutPutFileName2 = ['propofol_',freqlabel{jj},'_Gb_pre_ind_post'];
    R2 = zeros(16,1);
    RMSE = zeros(16,1);
    
    for ii = 1:length(subjects)
        %prepare for data input
        InPutFileName = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_global_',freqlabel{jj},'_LZW.mat'];

        VarName = ['rho0_',freqlabel{jj}]; 
        
        %load continuous data
        load([datadir,InPutFileName])
        lzw_whole = eval(VarName);
        lzw = lzw_whole(1,1:1391);
        lzw(1,1392) = lzw(1,1391);
        LZW_All = cat(1,LZW_All,lzw);
        
        time_x = [0:5:116*60-1]; %10+48+10 = 68
        time = [0:116*60-1];


        
        %fit curve
        p = fittype('poly3');% Cubic curve-fit
        [f,gof] = fit(time_x',lzw',p);
        R2(ii,1) = gof.adjrsquare;
        RMSE(ii,1) = gof.rmse;
        
        %Plot figure
        % Create subplot
        subplot1 = subplot(4,4,ii,'Parent',figure1)
        hold(subplot1,'on');
        % Activate the left side of the axes
        yyaxis(subplot1,'left');
        % Create plot
        plot(time_x,lzw,'DisplayName','LZW',...
            'Parent',subplot1,...
            'Marker','.',...
            'LineStyle','none',...
            'Color',[0 0.447 0.741]);
        % Create ylabel          
        ylabel('Global complexity (\rho0)');

        % Set the remaining axes properties(left yaxis color)
        set(subplot1,'YColor',[0 0.447 0.741],...
             'YTick',[0:0.2:1.2],...
             'YTickLabel',{'0','0.2','0.4','0.6',...
             '0.8','1.0','1.2'});

        % Activate the right side of the axes
        yyaxis(subplot1,'right');
        % Create plot
        plot(time,fxconc,'DisplayName','Conc',...
            'Parent',subplot1,...
            'LineWidth',2);

        % Create plot
        if ii == 10 | ii == 11
            disp('fail to hit swas')
        else
            plot([TSWAS(ii,1),TSWAS(ii,1)],[0,6],'DisplayName','TSWAS',...
                'Parent',subplot1,...
                'LineWidth',2.5,...
                'LineStyle','-',...
                'Color',[0 0 0]);
        end

        % Create plot
        plot([LOBR(ii,1),LOBR(ii,1)],[0,6],'DisplayName','LOBR',...
            'Parent',subplot1,...
            'LineWidth',2.5,...
            'LineStyle',':',...
            'Color',[0 0 0]);
        
        % Create plot
        plot([ROBR(ii,1),ROBR(ii,1)],[0,6],'DisplayName','ROBR',...
            'Parent',subplot1,...
            'LineWidth',2.5,...
            'LineStyle','-.',...
            'Color',[0 0 0]);
        
        
        % Create ylabel
        ylabel('Propofol ESC (\mug/ml)');

        % Set the remaining axes properties
        set(subplot1,'YColor',[0.85 0.325 0.098]);
        % Create xlabel
        xlabel('time (min)');

        % Create title
        title(['Sub ',num2str(ii)]);

        % Set the remaining axes properties
        set(subplot1,'FontSize',18,...
            'XTick',...
            [0 300 600 900 1200 1500 1800 2100 2400 2700 3000 3300 3600 3900 ...
            4200 4500 4800 5100 5400 5700 6000 6300 6600 6900 7200 7500],...
            'XTickLabel',...
            {'','5','','','','25','','','','45','','','','65',...
            '','','','85','','','','105','','','','125'});

    end
    R2_all(:,jj) = R2;
    RMSE_all(:,jj) = RMSE;
    
end

