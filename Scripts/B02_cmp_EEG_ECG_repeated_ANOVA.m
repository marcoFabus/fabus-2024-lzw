addpath('/Volumes/Seagate Basic/Scripts_func');
clear;clc;
%% EEG_LZW
datadir = '/Volumes/Seagate Basic/preprocess_global_LZW/';
subjects = {'001','003','004','005','007','008',...
    '011','013','014','016','017','019',...
    '020','021','022','023'};
LZW_All = [];

for ii = 1:length(subjects)
    %prepare for data input
    InPutFileName = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_global_broad_LZW.mat'];
    VarName = ['rho0_broad'];
    %load continuous data
    load([datadir,InPutFileName])
    lzw_whole = eval(VarName);
    lzw = lzw_whole(1,1:1391);
    lzw(1,1392) = lzw_whole(1,1391);
    LZW_All = cat(1,LZW_All,lzw);
end

EEG_Awake = LZW_All(:,1:120);
EEG_Peak = LZW_All(:,697:816);

%% EKG_LZW
datadir = '/Volumes/Seagate Basic/preprocess_global_EKG_LZW/';
subjects = {'001','003','004','005','007','008',...
    '011','013','014','016','017','019',...
    '020','021','022','023'};
EKG_All = [];
for ii = 1:length(subjects)
    %prepare for data input
    InPutFileName = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_global_broad_LZW.mat'];
    VarName = ['rho0_broad'];
    %load continuous data
    load([datadir,InPutFileName])
    lzw_whole = eval(VarName);
    lzw = lzw_whole(1,1:1391);
    lzw(1,1392) = lzw_whole(1,1391);
    EKG_All = cat(1,EKG_All,lzw);
end
EKG_Awake = EKG_All(:,1:120);
EKG_Peak = EKG_All(:,697:816);

%% Organize data for plotting
type = {'EEG','EEG','ECG','ECG'};
stage = {'Awake','Peak','Awake','Peak'};
Group.LZW = [];
Group.DataType = {};
Group.StageName = {};
Group.LZW = cat(1,mean(EEG_Awake,2),mean(EEG_Peak,2),mean(EKG_Awake,2),mean(EKG_Peak,2));
for idx = 1:4
Group.DataType((16*idx-15):16*idx,1) = type(idx);
Group.StageName((16*idx-15):16*idx,1) = stage(idx);
end


g(1,2) = gramm('x',Group.StageName,'y',Group.LZW,'color',Group.StageName,'subset',strcmp(Group.DataType,'EEG'));
g(1,2).stat_boxplot();
g(1,2).set_names('y','Global Complexity','x',{''},'color',{'Stage'});
g(1,2).set_text_options('font','Arial','base_size',25);
g(1,2).set_title('');
g(1,2).axe_property('YLim',[0 1.2],'YTick',[0:0.2:1.2]);
%g(1,2).set_order_options('x',{'EEG','ECG'});
g(1,2).draw();

EEG_Awake = Group.LZW(strcmp(Group.DataType,'EEG')&strcmp(Group.StageName,'Awake'));
EEG_Peak = Group.LZW(strcmp(Group.DataType,'EEG')&strcmp(Group.StageName,'Peak'));
ECG_Awake = Group.LZW(strcmp(Group.DataType,'ECG')&strcmp(Group.StageName,'Awake'));
ECG_Peak = Group.LZW(strcmp(Group.DataType,'ECG')&strcmp(Group.StageName,'Peak'));

Test = struct();
[Test(1,1).h Test(1,1).p Test(1,1).ci Test(1,1).stats] = ttest(EEG_Awake,ECG_Awake);
[Test(2,1).h Test(2,1).p Test(2,1).ci Test(2,1).stats] = ttest(EEG_Peak,ECG_Peak);
Test2 = struct();
[Test2(1,1).h Test2(1,1).p Test2(1,1).ci Test2(1,1).stats] = ttest2(EEG_Awake,EEG_Peak);
[Test2(2,1).h Test2(2,1).p Test2(2,1).ci Test2(2,1).stats] = ttest2(ECG_Awake,ECG_Peak);

EEG_all = cat(1,EEG_Awake,EEG_Peak);
ECG_all = cat(1,ECG_Awake,ECG_Peak);
[h p] = ttest2(EEG_all,ECG_all);

%% repeated anova
Y_LZW = [];
nb = 4;
Y_LZW = cat(2,EEG_Awake,EEG_Peak,ECG_Awake,ECG_Peak);

VariableName = {'Chan','Stage'};
FreqBand_table = {'EEG','EEG','ECG','ECG'}';
Stage_table = {'Awake','Peak','Awake','Peak'}';
within = table(FreqBand_table,Stage_table ,'VariableNames', VariableName);
Y_LZW = array2table(Y_LZW);
%fit the repeated measures model
rm = fitrm(Y_LZW,'Y_LZW1-Y_LZW4~1','WithinDesign',within);
[ranovatblb] = ranova(rm, 'WithinModel','Chan*Stage');
Mrm3 = multcompare(rm,'Stage','By','Chan','ComparisonType','bonferroni');
withins2 = within;
withins2.Chan = categorical(withins2.Chan);
withins2.Stage = categorical(withins2.Stage);
withins2.Chan_Stage = withins2.Chan .* withins2.Stage;
%%run my repeated measures anova for Directional Biases
rm2 = fitrm(Y_LZW,'Y_LZW1-Y_LZW4~1','WithinDesign',withins2); % overal fit
[ranovatblb2] = ranova(rm2, 'WithinModel', 'Chan*Stage');
Mrm4 = multcompare(rm2,'Chan','By','Stage','ComparisonType','bonferroni');
Mrm5 = multcompare(rm2,'Stage','By','Chan');
Mrm5 = multcompare(rm2,'Stage','By','Chan','ComparisonType','bonferroni');