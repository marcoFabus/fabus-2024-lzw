% This is a script I used to generate global data in whole anaesthsia
% This script is used to illustrate the method for generating global data
%% Load Data
% These data are preprocessed by BrainVision Analyzer(ocular ICA)
% and transformed to .mat by EEGLAB's 'pop_loadbv' function
clear;clc;
%% Set Path
addpath('/Volumes/Seagate Basic/LZW_Phase_shuffling_v2');
addpath('/Volumes/Seagate Basic/Scripts_func');
datadir = '/Volumes/Seagate Basic/mat';
subjects = {'001', '003', '004', '005',...
            '007', '008', '011', '013',...
            '014', '016', '017', '019',...
            '020', '021', '022','023'};
savedir_0 = '/Volumes/Seagate Basic/phaseshuffle/';

% Channel parameters
Chanlocs = [1:31];
Chanlabels = {'Fp1','Fp2','F3','F4','C3','C4',...
    'P3','P4','O1','O2','F7','F8','T7','T8','P7','P8',...
    'Fz','Cz','Pz','Oz','FC1','FC2','CP1','CP2','FC5','FC6',...
    'CP5','CP6','TP9','TP10','POz'};

%% Build Loop and load data
for pp = 1:3
    savedir = [savedir_0,num2str(pp),'/'];
    for ii = 1:length(subjects)
        %% Preparation
        filename = ['propofol_',subjects{ii},'_bench_all_ocICA_avg.mat'];          
        newfilename = ['propofol_',subjects{ii},'_bench_br_ocICA_avg_prep_global_'];
        load([datadir,filename]);
        broad_global_shuffled = [];
        %% Phase shuffle global data
        n_permutations = 10;
        
        for s = 1:n_permutations
            
            %% Initial global var for each subject in each frequency band
            broad_global = [];
            
            for jj = 1:length(Chanlocs)
                %display indication
                disp(['Subject: ',subjects{ii}])
                disp('   Loading data...')

                eeg = EEG.data;
                eeg = double(eeg);
                Fs = EEG.srate; %sampling rate
                srate = round(EEG.srate); %sampling rate in integer format
                %% Set local electrodes
                chanloc = Chanlocs(jj); % eg. Fz match the number 17
                eeg = eeg(chanloc,:); %select one channel data
                %% Notch and Down Sampling
                % notch fileter
                AC_Fs = 50; %60 in North American, 50 in Europe
                wo = AC_Fs/(Fs/2);
                Q_Value = 35; % test number between 1:35
                bw = wo/Q_Value;
                [b,a] = iirnotch(wo,bw);
                eeg = filtfilt(b,a,eeg);
                
                %% Extracting frequency bands
                % Using the function LZW_butterBandpass
                disp('   Extracting frequency bands...')
                broad = LZW_butterBandpass(eeg,[0.5 90],2,Fs);
%                 slow = LZW_butterBandpass(eeg,[0.5 1.5],2,Fs);
%                 delta = LZW_butterBandpass(eeg,[1.5 4],2,Fs);
%                 theta = LZW_butterBandpass(eeg,[4 8],2,Fs);
%                 alpha = LZW_butterBandpass(eeg,[8 14],2,Fs);
%                 beta = LZW_butterBandpass(eeg,[14 30],2,Fs);
%                 gamma = LZW_butterBandpass(eeg,[30 90],2,Fs);
                %% Windowing data
                disp(' Window sizing data')
                %set parameters of the windowing function
                %we decided to use 9s for window-length and 4s for overlap-length
                l_window = 9;
                l_overlap = 4;
                
                % Windowing data by the customized function
                % 'LZW_Windowing_shuffle' # NEW FROM MSF, shuffle each channel
                % separately
                
                broad_windowed = LZW_Windowing_shuffle(broad,l_window,l_overlap,srate);
%                 slow_windowed = LZW_Windowing_shuffle(slow,l_window,l_overlap,srate);
%                 delta_windowed = LZW_Windowing_shuffle(delta,l_window,l_overlap,srate);
%                 theta_windowed = LZW_Windowing_shuffle(theta,l_window,l_overlap,srate);
%                 alpha_windowed = LZW_Windowing_shuffle(alpha,l_window,l_overlap,srate);
%                 beta_windowed = LZW_Windowing_shuffle(beta,l_window,l_overlap,srate);
%                 gamma_windowed = LZW_Windowing_shuffle(gamma,l_window,l_overlap,srate);
                
                %% Concatenate all channels to global data
                
                broad_global = cat(3,broad_global,broad_windowed);
%                 slow_global = cat(3,slow_global,slow_windowed);
%                 delta_global = cat(3,delta_global,delta_windowed);
%                 theta_global = cat(3,theta_global,theta_windowed);
%                 alpha_global = cat(3,alpha_global,alpha_windowed);
%                 beta_global = cat(3,beta_global,beta_windowed);
%                 gamma_global = cat(3,gamma_global,gamma_windowed);
                
            end
            
            broad_global_shuffled(s, :, :) = squeeze(broad_global);
%             slow_global_shuffled(s, :, :) = squeeze(slow_global);
            
        end
        
        %% Save Output
        %     save([savedir,newfilename,'broad'],'broad_global');
        %     save([savedir,newfilename,'slow'],'slow_global');
        %     save([savedir,newfilename,'delta'],'delta_global');
        %     save([savedir,newfilename,'theta'],'theta_global');
        %     save([savedir,newfilename,'alpha'],'alpha_global');
        %     save([savedir,newfilename,'beta'],'beta_global');
        %     save([savedir,newfilename,'gamma'],'gamma_global');
        %
        % Save shuffled
        save([savedir,newfilename,'broad_shuffled'],'broad_global_shuffled','-v7.3');
%         save([savedir,newfilename,'slow_shuffled'],'slow_global_shuffled');
    end
end
