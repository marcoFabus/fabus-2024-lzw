clear; clc;
%% EEG_LZW
datadir = '/Volumes/Seagate Basic/preprocess_local_LZW/';
subjects = {'001','003','004','005','007','008',...
    '011','013','014','016','017','019',...
    '020','021','022','023'};

%load Channel location file
load('/Volumes/Seagate Basic/Chanlocs.mat');%Ch_Map is the default name
Chanlocs = Ch_Map(1,[1:31]);

% Load TSWAS and LOBR timepoints
load('/Volumes/Seagate Basic/KeyPoints/TSWAS.mat')
load('/Volumes/Seagate Basic/KeyPoints/LOBR.mat')
load('/Volumes/Seagate Basic/KeyPoints/ROBR.mat')
TSWAS = TSWAS+60*10; TSWAS_mk = ceil(TSWAS/5);
LOBR = LOBR+60*10; LOBR_mk = ceil(LOBR/5);
ROBR = ROBR+60*68; ROBR_mk = ceil(ROBR/5);

%% Whole length of LOBR
LZW_All = [];

for ii = 1:length(subjects)
    %prepare for data input
    InPutFileName = ['propofol_',subjects{ii},'_bench_all_ocICA_avg_prep_local_broad_LZW.mat'];
    VarName = ['rho0_broad'];
    %load continuous data
    load([datadir,InPutFileName])
    lzw_whole = eval(VarName);
    lzw = lzw_whole(:,1:1391);
    lzw(1,1392) = lzw(1,1391);
    LZW_All(ii,:,:) = lzw;
end
%% 01_Beginning of the experiment 10-min Pre
LZW_Begin_Onset = LZW_All(:,:,1:120);
%% 02_From Anesthesia Onset to LOBR
LZW_Onset_LOBR = NaN(16,31,max(LOBR_mk)-120);
End_LOBR_mk = mean(LOBR_mk)-120;
for ii = 1:16
    LZW_Onset_LOBR(ii,:,1:LOBR_mk(ii)-120) = LZW_All(ii,:,121:LOBR_mk(ii));
end
%% 03_From LOBR to TSWAS
LZW_LOBR_TSWAS = NaN(16,31,max(-LOBR_mk+TSWAS_mk));
End_TSWAS_mk = mean(-LOBR_mk+TSWAS_mk);
for ii = [1:9 12:16]
    LZW_LOBR_TSWAS(ii,:,1:1-LOBR_mk(ii)+TSWAS_mk(ii)) = LZW_All(ii,:,LOBR_mk(ii):TSWAS_mk(ii));
end
%% 04_From TSWAS to The start of the Peak-Anesthesia
TSWAS_mk2 = TSWAS_mk([1:9 12:16],:);
LZW_LOBR_Peak = NaN(16,31,max(696-TSWAS_mk2));
End_Peak_mk = mean(696-TSWAS_mk2);
for ii = [1:9 12:16]
    LZW_LOBR_Peak(ii,:,1:length(TSWAS_mk(ii):696)) = LZW_All(ii,:,TSWAS_mk(ii):696);
end
%% 05_Peak Anesthesia
LZW_Peak_Anes = LZW_All(:,:,697:816);
%% 06_From Peak to ROBR
LZW_Peak_ROBR = NaN(16,31,max(ROBR_mk-816));
End_ROBR_mk = mean(ROBR_mk-816);
for ii = 1:16
    LZW_Peak_ROBR(ii,:,1:length(817:ROBR_mk(ii))) = LZW_All(ii,:,817:ROBR_mk(ii));
end
%% 07_From ROBR to END
LZW_ROBR_End = NaN(16,31,max(1392-ROBR_mk));
End_End_mk = mean(1392-ROBR_mk);
for ii = 1:16
    LZW_ROBR_End(ii,:,1:length(ROBR_mk(ii):1392)) = LZW_All(ii,:,ROBR_mk(ii):1392);
end

%% Topo-Plot
TP_01 = cal_TP(LZW_Begin_Onset);
TP_02 = cal_TP(LZW_Onset_LOBR);
TP_03 = cal_TP(LZW_LOBR_TSWAS);
TP_04 = cal_TP(LZW_LOBR_Peak);
TP_05 = cal_TP(LZW_Peak_Anes);
TP_06 = cal_TP(LZW_Peak_ROBR);
TP_07 = cal_TP(LZW_ROBR_End);

set_pos = 1.2;
subplot(1,7,1)
topoplot(TP_01,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));
LZW_topoplot_setpos(set_pos);

subplot(1,7,2)
topoplot(TP_02,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));
LZW_topoplot_setpos(set_pos);

subplot(1,7,3)
topoplot(TP_03,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));
LZW_topoplot_setpos(set_pos);

subplot(1,7,4)
topoplot(TP_04,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));
LZW_topoplot_setpos(set_pos);

subplot(1,7,5)
topoplot(TP_05,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));
LZW_topoplot_setpos(set_pos);

subplot(1,7,6)
topoplot(TP_06,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));
LZW_topoplot_setpos(set_pos);

subplot(1,7,7)
topoplot(TP_07,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));
LZW_topoplot_setpos(set_pos);




for ii = 1:16
    LZW_LOBR_mk(ii,:) = LZW_All(ii,:,LOBR_mk(ii));
    LZW_ROBR_mk(ii,:) = LZW_All(ii,:,ROBR_mk(ii)); 
end
TP_LOBR = cal_TP(LZW_LOBR_mk);
TP_ROBR = cal_TP(LZW_ROBR_mk);
topoplot(TP_LOBR,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));

topoplot(TP_ROBR,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));



LZW_TSWAS_mk = NaN(16,31);
for ii = [1:9 12:16]
    LZW_TSWAS_mk(ii,:) = LZW_All(ii,:,TSWAS_mk(ii));
end
TP_TSWAS = cal_TP(LZW_TSWAS_mk);
topoplot(TP_TSWAS,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));


%% Calculate the correlation matrix
for ii = 1:16
    LZW_LOBR_mk_P5(ii,:) = LZW_All(ii,:,LOBR_mk(ii)+60);
    LZW_LOBR_mk_N5(ii,:) = LZW_All(ii,:,LOBR_mk(ii)-60); 
end
TP_LOBR_P5 = cal_TP(LZW_LOBR_mk_P5);
TP_LOBR_N5 = cal_TP(LZW_LOBR_mk_N5);

figure;
topoplot(TP_LOBR_P5,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));
figure;
topoplot(TP_LOBR_N5,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));


for ii = 1:16
    LZW_ROBR_mk_P5(ii,:) = LZW_All(ii,:,ROBR_mk(ii)+60);
    LZW_ROBR_mk_N5(ii,:) = LZW_All(ii,:,ROBR_mk(ii)-60); 
end
TP_ROBR_P5 = cal_TP(LZW_ROBR_mk_P5);
TP_ROBR_N5 = cal_TP(LZW_ROBR_mk_N5);

figure;
topoplot(TP_ROBR_P5,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));
figure;
topoplot(TP_ROBR_N5,Chanlocs,'electrodes','on','maplimits',[-1.5,1.5],'colormap',turbo(32));

